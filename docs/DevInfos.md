# Entwickler-Informationen

Ich habe versucht das Modul soweit wie möglich offen für weitere zu schützende Erweiterungen zuhalten.

Es wird, wenn die Freigabeverwaltung für den Bereich aktiv ist, werden Callbacks geladen. Er versucht erst eine Methode "onEditCallback" und eine Methode "onSubmitCallback" in einer Klasse mit folgenden Namensaufbau zufinden [table].'_rms'. Sollte es diese nicht geben greift er auf die gleichnamigen Methoden in der rmsDefaultCallbacks.php zurück. Es muss hier also keine eigene Logik vorhanden sein, wenn die Funktion so wie sie vorhanden ist genügt.
(Quelle: rmsHelper->handleBackendUserAccessControlls())

Weiterhin gibt es zusätzliche HOOKS um zu pruefen ob Inhalte Freigabegeschützt sind. Diese werden zwingend für eigene Erweiterungen benötigt:

1. $GLOBALS['TL_HOOKS'] ['rmsIsContentProtected'] Falls eine Erweiterung auch auf die tl_content-Tabelle zurückgreift. Übergibt die aktuelle Tabelle und überprüft selbst auf den GET-Paramter do
2. $GLOBALS['TL_HOOKS'] ['rmsIsTableProtected'] Wird für alle Erweiterungs-DCA benötigt welche nicht tl_content sind. Übergibt die aktuelle Tabelle und überprüft auf die übergebene Tabelle
3. Für bestimmte Module gibt es für die Frontendausgabe bestimmte zusätzliche Formatierungen die beim überschreiben der rms-Daten verloren gehen bevor diese ans Template geschickt werden. Für diesen Fall prüfe ich ob es eine Methode 'modifyForPreview' in der Class $objTemplate->rms_ref_table.'_rms' existiert. Als Beispiel für die News lege ich die Methode modifyForPreview in der Klasse tl_news_rms an und setzte dort einen Template-Parameter 'linkHeadline' neu.
