# ChangeLog for contao-rms-bundle

## 0.15.9 (17.02.2023)
- fix array key in dca-files with Contao 4.13.15 and php 8.1
- 
## 0.15.8 (17.02.2023)
- fix array key in dca-files with Contao 4.13.15 and php 8.1

## 0.15.7 (18.05.2022)
- fix RmsVersions.php: set second parameter from explode() as string
- fix Rms.php: make uuidToBin() if value a uuid
- fix PostFieldSaveCallbackListener.php: not overwrite alias with empty value

## 0.15.6 (19.04.2022)
- fix exclores in preview

## 0.15.5 (19.04.2022)
- fix title in list-view from new elements
- fix save images etc in the second step

## 0.15.4 (24.03.2022)
- fix first ModuleEventReaderRms::getDateAndTime() Parameter
- fis date-fields to (int) in Rms::acknowledgeEntry()

## 0.15.3 (18.03.2022)
- fix: add (int) for date-fields ModuleNewsReaderRms, ModuleEventReaderModule

## 0.15.2 (06.03.2022)
- fix: Rms::acknowledgeEntry() strtotime() only if $value a string and not ''

## 0.15.1 (28.02.2022)
- fix: Rms::acknowledgeEntry() strtotime() only if $value a string

## 0.15.0 (27.02.2022)
- fix Alias in Previewlinks (auch beim ersten save) und Preview-Darstellung mit Content-Elementen in News wenn do=preview

## 0.14.2 (25.02.2022)
- fix Alias in Previewlinks und Preview-Darstellung wenn do=preview

## 0.14.1 (07.10.2021)
- run codefixer (without RmsTable.php)

## 0.14.0 (16.09.2021)
- fixed #1: Variable methods must be in brackets (Tim Gatzky <info@tim-gatzky.de> on 27.06.21 at 15:04)
- fix RmsLog.php: deserialize as array before compare() dat
- fix Rms.php: convert time,date datim fields as timestamp before overwrite in DB
- fix be_rmsdiff.html5: test firstSave on String not on NULL

## 0.13.0 (bis 15.09.2021)
-- Das Freigabemodul mit Logging und diff-Feld für Frontendseitige Auswertung der gemachten Änderungen (noch fehlerhaft)