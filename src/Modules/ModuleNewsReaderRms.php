<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension contao-rms-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

namespace Srhinow\ContaoRmsBundle\Modules;

use Contao\BackendTemplate;
use Contao\Comments;
use Contao\Config;
use Contao\ContentModel;
use Contao\CoreBundle\Exception\InternalServerErrorException;
use Contao\CoreBundle\Exception\PageNotFoundException;
use Contao\Environment;
use Contao\FilesModel;
use Contao\FrontendTemplate;
use Contao\Input;
use Contao\ModuleNewsReader;
use Contao\News;
use Contao\NewsArchiveModel;
use Contao\PageModel;
use Contao\StringUtil;
use Contao\System;
use Contao\UserModel;
use Patchwork\Utf8;
use Srhinow\ContaoRmsBundle\Helper\RmsHelper;
use Srhinow\ContaoRmsBundle\Model\NewsModel;
use Srhinow\ContaoRmsBundle\Model\RmsModel;

/**
 * Front end module "news reader".
 *
 * @property Comments $Comments
 * @property string   $com_template
 * @property array    $news_archives
 */
class ModuleNewsReaderRms extends ModuleNewsReader
{
    /**
     * Display a wildcard in the back end.
     *
     * @throws InternalServerErrorException
     *
     * @return string
     */
    public function generate()
    {
        $request = System::getContainer()->get('request_stack')->getCurrentRequest();

        if ($request && System::getContainer()->get('contao.routing.scope_matcher')->isBackendRequest($request))
        {
            $objTemplate = new BackendTemplate('be_wildcard');
            $objTemplate->wildcard = '### ' . Utf8::strtoupper($GLOBALS['TL_LANG']['FMD']['newsreader'][0]) . ' ###';
            $objTemplate->title = $this->headline;
            $objTemplate->id = $this->id;
            $objTemplate->link = $this->name;
            $objTemplate->href = 'contao/main.php?do=themes&amp;table=tl_module&amp;act=edit&amp;id=' . $this->id;

            return $objTemplate->parse();
        }

        // Set the item from the auto_item parameter
        if (!isset($_GET['items']) && isset($_GET['auto_item']) && Config::get('useAutoItem'))
        {
            Input::setGet('items', Input::get('auto_item'));
        }

        // Return an empty string if "items" is not set (to combine list and reader on same page)
        if (!Input::get('items'))
        {
            return '';
        }

        $this->news_archives = $this->sortOutProtected(StringUtil::deserialize($this->news_archives));

        if (empty($this->news_archives) || !\is_array($this->news_archives))
        {
            throw new InternalServerErrorException('The news reader ID ' . $this->id . ' has no archives specified.');
        }

        return parent::generate();
    }

    /**
     * Generate the module.
     */
    protected function compile(): void
    {
        /* @var PageModel $objPage */
        global $objPage;

        $this->Template->articles = '';
        $this->Template->referer = 'javascript:history.go(-1)';
        $this->Template->back = $GLOBALS['TL_LANG']['MSC']['goBack'];

        // Get the news item
        $objArticle = NewsModel::findPublishedByParentAndIdOrAlias(Input::get('items'), $this->news_archives);

        // The news item does not exist or has an external target (see #33)
        if (null === $objArticle || 'default' !== $objArticle->source) {
            throw new PageNotFoundException('Page not found: '.Environment::get('uri'));
        }

        if ('preview' === Input::get('do')) {
            $objStoredData = RmsModel::findRef('tl_news', $objArticle->id);

            if (null !== $objStoredData) {
                $RmsHelper = RmsHelper::getInstance();
                $objArticle = $RmsHelper->overwriteDbObj(
                    $objArticle,
                    StringUtil::deserialize($objStoredData->data)
                );
            }
        }

        // Set the default template
        if (!$this->news_template) {
            $this->news_template = 'news_full';
        }

        $arrArticle = $this->parseArticle($objArticle);
        $this->Template->articles = $arrArticle;

        // Overwrite the page title (see #2853, #4955 and #87)
        if ($objArticle->pageTitle) {
            $objPage->pageTitle = $objArticle->pageTitle;
        } elseif ($objArticle->headline) {
            $objPage->pageTitle = strip_tags(StringUtil::stripInsertTags($objArticle->headline));
        }

        // Overwrite the page description
        if ($objArticle->description) {
            $objPage->description = $objArticle->description;
        } elseif ($objArticle->teaser) {
            $objPage->description = $this->prepareMetaDescription($objArticle->teaser);
        }

    }

    /**
     * Parse an item and return it as string
     *
     * @param \Contao\NewsModel $objArticle
     * @param boolean   $blnAddArchive
     * @param string    $strClass
     * @param integer   $intCount
     *
     * @return string
     */
    protected function parseArticle($objArticle, $blnAddArchive=false, $strClass='', $intCount=0)
    {
        $objTemplate = new FrontendTemplate($this->news_template ?: 'news_latest');
        $objTemplate->setData($objArticle->row());

        if ($objArticle->cssClass)
        {
            $strClass = ' ' . $objArticle->cssClass . $strClass;
        }

        if ($objArticle->featured)
        {
            $strClass = ' featured' . $strClass;
        }

        $objTemplate->class = $strClass;
        $objTemplate->newsHeadline = $objArticle->headline;
        $objTemplate->subHeadline = $objArticle->subheadline;
        $objTemplate->hasSubHeadline = $objArticle->subheadline ? true : false;
        $objTemplate->linkHeadline = $this->generateLink($objArticle->headline, $objArticle, $blnAddArchive);
        $objTemplate->more = $this->generateLink($GLOBALS['TL_LANG']['MSC']['more'], $objArticle, $blnAddArchive, true);
        $objTemplate->link = News::generateNewsUrl($objArticle, $blnAddArchive);
        $objTemplate->archive = $objArticle->getRelated('pid');
        $objTemplate->count = $intCount; // see #5708
        $objTemplate->text = '';
        $objTemplate->hasText = false;
        $objTemplate->hasTeaser = false;

        // Clean the RTE output
        if ($objArticle->teaser)
        {
            $objTemplate->hasTeaser = true;
            $objTemplate->teaser = StringUtil::toHtml5($objArticle->teaser);
            $objTemplate->teaser = StringUtil::encodeEmail($objTemplate->teaser);
        }

        // Display the "read more" button for external/article links
        if ($objArticle->source != 'default')
        {
            $objTemplate->text = true;
            $objTemplate->hasText = true;
        }

        // Compile the news text
        else
        {
            $id = $objArticle->id;

            $objTemplate->text = function () use ($id)
            {
                $strText = '';
                $objElements = ContentModel::findPublishedByPidAndTable($id, 'tl_news');

                if ($objElements !== null)
                {
                    while ($objElements->next())
                    {
                        $objElement = $objElements->current();

                        if ('preview' === Input::get('do')) {
                            $objStoredData = RmsModel::findRef('tl_content', $objElement->id);
                            if (null !== $objStoredData) {
                                $RmsHelper = RmsHelper::getInstance();
                                $objElement = $RmsHelper->overwriteDbObj(
                                    $objElement,
                                    StringUtil::deserialize($objStoredData->data)
                                );
                            }
                        }

                        $strText .= $this->getContentElement($objElement);

                    }
                }

                return $strText;
            };

            $objTemplate->hasText = static function () use ($objArticle)
            {
                return ContentModel::countPublishedByPidAndTable($objArticle->id, 'tl_news') > 0;
            };
        }

        $arrMeta = $this->getMetaFields($objArticle);

        // Add the meta information
        $objTemplate->date = $arrMeta['date'];
        $objTemplate->hasMetaFields = !empty($arrMeta);
        $objTemplate->numberOfComments = $arrMeta['ccount'];
        $objTemplate->commentCount = $arrMeta['comments'];
        $objTemplate->timestamp = $objArticle->date;
        $objTemplate->author = $arrMeta['author'];
        $objTemplate->datetime = date('Y-m-d\TH:i:sP', (int) $objArticle->date);

        $objTemplate->addImage = false;

        // Add an image
        if ($objArticle->addImage && $objArticle->singleSRC)
        {
            $objModel = FilesModel::findByUuid($objArticle->singleSRC);

            if ($objModel !== null && is_file(System::getContainer()->getParameter('kernel.project_dir') . '/' . $objModel->path))
            {
                // Do not override the field now that we have a model registry (see #6303)
                $arrArticle = $objArticle->row();

                // Override the default image size
                if ($this->imgSize)
                {
                    $size = StringUtil::deserialize($this->imgSize);

                    if ($size[0] > 0 || $size[1] > 0 || is_numeric($size[2]) || ($size[2][0] ?? null) === '_')
                    {
                        $arrArticle['size'] = $this->imgSize;
                    }
                }

                $arrArticle['singleSRC'] = $objModel->path;
                $this->addImageToTemplate($objTemplate, $arrArticle, null, null, $objModel);

                // Link to the news article if no image link has been defined (see #30)
                if (!$objTemplate->fullsize && !$objTemplate->imageUrl)
                {
                    // Unset the image title attribute
                    $picture = $objTemplate->picture;
                    unset($picture['title']);
                    $objTemplate->picture = $picture;

                    // Link to the news article
                    $objTemplate->href = $objTemplate->link;
                    $objTemplate->linkTitle = StringUtil::specialchars(sprintf($GLOBALS['TL_LANG']['MSC']['readMore'], $objArticle->headline), true);

                    // If the external link is opened in a new window, open the image link in a new window, too (see #210)
                    if ($objTemplate->source == 'external' && $objTemplate->target && strpos($objTemplate->attributes, 'target="_blank"') === false)
                    {
                        $objTemplate->attributes .= ' target="_blank"';
                    }
                }
            }
        }

        $objTemplate->enclosure = array();

        // Add enclosures
        if ($objArticle->addEnclosure)
        {
            $this->addEnclosuresToTemplate($objTemplate, $objArticle->row());
        }

        // HOOK: add custom logic
        if (isset($GLOBALS['TL_HOOKS']['parseArticles']) && \is_array($GLOBALS['TL_HOOKS']['parseArticles']))
        {
            foreach ($GLOBALS['TL_HOOKS']['parseArticles'] as $callback)
            {
                $this->import($callback[0]);
                $this->{$callback[0]}->{$callback[1]}($objTemplate, $objArticle->row(), $this);
            }
        }

        // Tag the news (see #2137)
        if (System::getContainer()->has('fos_http_cache.http.symfony_response_tagger'))
        {
            $responseTagger = System::getContainer()->get('fos_http_cache.http.symfony_response_tagger');
            $responseTagger->addTags(array('contao.db.tl_news.' . $objArticle->id));
        }

        return $objTemplate->parse();
    }
}
