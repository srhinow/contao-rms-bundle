<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension contao-rms-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

namespace Srhinow\ContaoRmsBundle\Modules;

use Contao\BackendTemplate;
use Contao\CoreBundle\Exception\PageNotFoundException;
use Contao\Environment;
use Contao\Input;
use Contao\ModuleNewsletterReader;
use Contao\PageModel;
use Contao\StringUtil;
use Patchwork\Utf8;
use Srhinow\ContaoRmsBundle\Helper\RmsHelper;
use Srhinow\ContaoRmsBundle\Model\NewsletterModel;
use Srhinow\ContaoRmsBundle\Model\RmsModel;

/**
 * Front end module "newsletter reader".
 *
 * @property array $nl_channels
 */
class ModuleNewsletterReaderRms extends ModuleNewsletterReader
{
    /**
     * Template.
     *
     * @var string
     */
    protected $strTemplate = 'mod_newsletterreader';

    /**
     * Display a wildcard in the back end.
     *
     * @return string
     */
    public function generate()
    {
        if (TL_MODE === 'BE') {
            $objTemplate = new BackendTemplate('be_wildcard');
            $objTemplate->wildcard = '### '
                .Utf8::strtoupper($GLOBALS['TL_LANG']['FMD']['newsletterreader'][0])
                .' - RMS-Preview ###';
            $objTemplate->title = $this->headline;
            $objTemplate->id = $this->id;
            $objTemplate->link = $this->name;
            $objTemplate->href = 'contao/main.php?do=themes&amp;table=tl_module&amp;act=edit&amp;id='.$this->id;

            return $objTemplate->parse();
        }

        return parent::generate();
    }

    /**
     * Generate the module.
     */
    protected function compile(): void
    {
        /* @var PageModel $objPage */
        global $objPage;

        $this->Template->content = '';
        $this->Template->referer = 'javascript:history.go(-1)';
        $this->Template->back = $GLOBALS['TL_LANG']['MSC']['goBack'];

        $objNewsletter = NewsletterModel::findSentByParentAndIdOrAlias(Input::get('items'), $this->nl_channels);

        if (null === $objNewsletter) {
            throw new PageNotFoundException('Page not found: '.Environment::get('uri'));
        }

        if ('preview' === Input::get('do')) {
            $objStoredData = RmsModel::findRef('tl_newsletter', $objNewsletter->id);

            if (null !== $objStoredData) {
                $RmsHelper = RmsHelper::getInstance();
                $objNewsletter = $RmsHelper->overwriteDbObj(
                    $objNewsletter,
                    StringUtil::deserialize($objStoredData->data)
                );
            }
        }

        // Overwrite the page title (see #2853 and #4955)
        if ('' !== $objNewsletter->subject) {
            $objPage->pageTitle = strip_tags(StringUtil::stripInsertTags($objNewsletter->subject));
        }

        // Add enclosure
        if ($objNewsletter->addFile) {
            $this->addEnclosuresToTemplate($this->Template, $objNewsletter->row(), 'files');
        }

        // Support plain text newsletters (thanks to Hagen Klemp)
        if ($objNewsletter->sendText) {
            $strContent = nl2br_html5($objNewsletter->text);
        } else {
            $strContent = str_ireplace(' align="center"', '', $objNewsletter->content);
        }

        // Parse simple tokens and insert tags
        $strContent = $this->replaceInsertTags($strContent);
        $strContent = StringUtil::parseSimpleTokens($strContent, []);

        // Encode e-mail addresses
        $strContent = StringUtil::encodeEmail($strContent);

        $this->Template->content = $strContent;
        $this->Template->subject = $objNewsletter->subject;
    }
}
