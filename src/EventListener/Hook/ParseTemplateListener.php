<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension contao-rms-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

namespace Srhinow\ContaoRmsBundle\EventListener\Hook;

use Contao\Input;
use Contao\StringUtil;
use Contao\System;
use Contao\Template;
use Srhinow\ContaoRmsBundle\Helper\RmsHelper;
use Srhinow\ContaoRmsBundle\Model\RmsModel;

class ParseTemplateListener
{
    public function onParseTemplate(Template $objTemplate): void
    {
        if ('preview' === Input::get('do')
            && 1 === $objTemplate->rms_new_edit
            && \strlen($objTemplate->rms_ref_table) > 0) {
            $this->modifyForPreview($objTemplate);
        }
    }

    /**
     * parseTemplate-HOOK
     * ersetzt die Inhalte mit rms-Datensatz, wenn rms_new_edit=1 und get-Parameter do=preview gesetzt ist.
     *
     * @param $objTemplate
     */
    public function modifyForPreview($objTemplate): void
    {
        // wenn es eine tl_content-Tabelle ist, nicht weiter
        // da diese Inhalte von 'previewContentElement' verarbeitet werden
        if (in_array($objTemplate->rms_ref_table,[
            'tl_content',
            'tl_news',
            'tl_calendar_event',
            'tl_newsletter'
        ])) {
            return;
        }

        $objStoredData = RmsModel::findRef($objTemplate->rms_ref_table, $objTemplate->id);
        if (null === $objStoredData) {
            return;
        }

        // custom edit for module template in the Dca-EventListener-class
        $sectionClass = 'srhinow.contao_rms_bundle.listener.dca.'.$objTemplate->rms_ref_table;
        $sectionMethod = 'modifyForPreview';
        $rmsDataArr = StringUtil::deserialize($objStoredData->data);

        if (method_exists($sectionClass, $sectionMethod)) {
            System::importStatic($sectionClass);
            $objTemplate = $this->{$sectionClass}->$sectionMethod($objTemplate, $rmsDataArr);
        } else {
            $RmsHelper = RmsHelper::getInstance();
            $objTemplate = $RmsHelper->overwriteDbObj($objTemplate, $rmsDataArr);
        }

        // force view
        $objTemplate->published = 1; // news,newsletter
        $objTemplate->invisible = 0; // content
    }
}
