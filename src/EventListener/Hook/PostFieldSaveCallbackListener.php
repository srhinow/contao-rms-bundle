<?php
/**
 * Created by rms-contao.
 * Developer: Sven Rhinow (sven@sr-tag.de)
 * Date: 25.02.22
 */

namespace Srhinow\ContaoRmsBundle\EventListener\Hook;

use Contao\BackendUser;
use Contao\CoreBundle\ServiceAnnotation\Hook;
use Contao\Input;
use Contao\StringUtil;
use Srhinow\ContaoRmsBundle\Model\RmsModel;
use Srhinow\ContaoRmsBundle\Model\RmsTmpModel;

class PostFieldSaveCallbackListener
{
    /**
     * @Hook("postFieldSaveCallback")
     */
    public function __invoke($oldvar, $newvar, $strTable, $strField)
    {

        // wenn der Wert, vor dem save_callback gleich dem ist wie nach dem save_callback, dann nicht weiter tun
        if($oldvar === $newvar) {
            return $newvar;
        }

        //hole nicht freigegebene Daten von dem Redakteur fuer diesen Content
        if (null === ($objRms = RmsModel::findRefByUser(
            $strTable,
                Input::get('id'),
            BackendUser::getInstance()->id))) {
            return $newvar;
        }

        $arrData = StringUtil::deserialize($objRms->data);
        if($strField !== 'alias' &&  $newvar !== '') {
            $arrData[$strField] = $newvar;
        }

        $objRms->data = serialize($arrData);
        $objRms->save();

        //hole nicht freigegebene Daten von dem Redakteur fuer diesen Content
        if (null === ($objTmpRms = RmsTmpModel::findRefByUser(
                $strTable,
                Input::get('id'),
                BackendUser::getInstance()->id))) {
            return $newvar;
        }

        $arrData = StringUtil::deserialize($objTmpRms->data);
        $arrData[$strField] = $newvar;

        $objTmpRms->data = serialize($arrData);
        $objTmpRms->save();

        return $newvar;
    }
}