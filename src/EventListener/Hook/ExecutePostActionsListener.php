<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension contao-rms-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

namespace Srhinow\ContaoRmsBundle\EventListener\Hook;

use Contao\BackendUser;
use Contao\Config;
use Contao\Controller;
use Contao\CoreBundle\Monolog\ContaoContext;
use Contao\Database;
use Contao\DataContainer;
use Contao\Dbafs;
use Contao\FileTree;
use Contao\Input;
use Contao\PageSelector;
use Contao\PageTree;
use Contao\StringUtil;
use Contao\System;
use ContaoCommunityAlliance\DcGeneral\Action;
use ContaoCommunityAlliance\DcGeneral\DC\General;
use Psr\Log\LogLevel;

class ExecutePostActionsListener
{
    /**
     * Ajax id.
     *
     * @var string
     */
    protected $strAjaxId;

    protected $Logger;

    protected $Database;

    /**
     * @var BackendUser
     */
    protected $User;

    /**
     * Ajax name.
     *
     * @var string
     */
    protected $strAjaxName;

    /**
     * @throws \Exception
     */
    public function onExecutePostActions(string $action, DataContainer $dc): void
    {
        // Bypass any core logic for non-core drivers (see #5957)
        if ($dc instanceof General) {
            $this->customRmsPostActions($action, $dc);
        }
    }

    /**
     * Ajax actions that do require a data container object.
     *
     * @throws \Exception
     */
    public function customRmsPostActions(string $strAction, DataContainer $dc): void
    {
        $Logger = Controller::getContainer()->get('monolog.logger.contao');
        $Database = Database::getInstance();

        header('Content-Type: text/html; charset='.Config::get('characterSet'));

        switch ($strAction) {
            // Load nodes of the page structure tree
            case 'loadStructure':
                echo $dc->ajaxTreeView($this->strAjaxId, (int) (Input::post('level')));
                exit;
                break;

            // Load nodes of the file manager tree
            case 'loadFileManager':
                echo $dc->ajaxTreeView(Input::post('folder', true), (int) (Input::post('level')));
                exit;
                break;

            // Load nodes of the page tree
            case 'loadPagetree':
                $arrData['strTable'] = $dc->table;
                $arrData['id'] = $this->strAjaxName ?: $dc->id;
                $arrData['name'] = Input::post('name');

                /** @var PageSelector $objWidget */
                $objWidget = new $GLOBALS['BE_FFL']['pageSelector']($arrData, $dc);
                echo $objWidget->generateAjax($this->strAjaxId, Input::post('field'), (int) (Input::post('level')));
                exit;
                break;

            // Load nodes of the file tree
            case 'loadFiletree':
                $arrData['strTable'] = $dc->table;
                $arrData['id'] = $this->strAjaxName ?: $dc->id;
                $arrData['name'] = Input::post('name');

                /** @var PageSelector $objWidget */
                $objWidget = new $GLOBALS['BE_FFL']['fileSelector']($arrData, $dc);

                // Load a particular node
                if ('' !== Input::post('folder', true)) {
                    echo $objWidget->generateAjax(
                        Input::post('folder', true),
                        Input::post('field'),
                        (int) (Input::post('level'))
                    );
                } else {
                    echo $objWidget->generate();
                }
                exit;
                break;

            // Reload the page/file picker
            case 'reloadPagetree':
            case 'reloadFiletree':
                $intId = Input::get('id');
                $strField = $dc->field = Input::post('name');

                // Handle the keys in "edit multiple" mode
                if ('editAll' === Input::get('act')) {
                    $intId = preg_replace('/.*_([0-9a-zA-Z]+)$/', '$1', $strField);
                    $strField = preg_replace('/(.*)_[0-9a-zA-Z]+$/', '$1', $strField);
                }

                // The field does not exist
                if (!isset($GLOBALS['TL_DCA'][$dc->table]['fields'][$strField])) {
                    $logtext = 'Field "'.$strField.'" does not exist in DCA "'.$dc->table.'"';
                    $Logger->log(
                        LogLevel::ERROR,
                        $logtext,
                        ['contao' => new ContaoContext(__METHOD__, TL_ERROR)]
                    );

                    header('HTTP/1.1 400 Bad Request');
                    die('Bad Request');
                }

                $objRow = null;
                $varValue = null;

                // Load the value
                if ('File' === $GLOBALS['TL_DCA'][$dc->table]['config']['dataContainer']) {
                    $varValue = $GLOBALS['TL_CONFIG'][$strField];
                } elseif ($intId > 0 && $Database->tableExists($dc->table)) {
                    $objRow = $Database
                        ->prepare('SELECT * FROM '.$dc->table.' WHERE '.$dc->table.'.id=?')
                        ->execute($intId)
                    ;

                    // The record does not exist
                    if ($objRow->numRows < 1) {
                        $logtext = 'A record with the ID "'.$intId.'" does not exist in table "'.$dc->table.'"';
                        $Logger->log(
                            LogLevel::ERROR,
                            $logtext,
                            ['contao' => new ContaoContext(__METHOD__, TL_ERROR)]
                        );
                        header('HTTP/1.1 400 Bad Request');
                        die('Bad Request');
                    }

                    $varValue = $objRow->$strField;
                    $dc->activeRecord = $objRow;
                }

                // Call the load_callback
                if (\is_array($GLOBALS['TL_DCA'][$dc->table]['fields'][$strField]['load_callback'])) {
                    foreach ($GLOBALS['TL_DCA'][$dc->table]['fields'][$strField]['load_callback'] as $callback) {
                        if (\is_array($callback)) {
                            System::importStatic($callback[0]);
                            $varValue = $this->$callback[0]->$callback[1]($varValue, $dc);
                        } elseif (\is_callable($callback)) {
                            $varValue = $callback($varValue, $dc);
                        }
                    }
                }

                // Set the new value
                $varValue = Input::post('value', true);
                $strKey = ('reloadPagetree' === $strAction) ? 'pageTree' : 'fileTree';

                // Convert the selected values
                if ('' !== $varValue) {
                    $varValue = StringUtil::trimsplit("\t", $varValue);

                    // Automatically add resources to the DBAFS
                    if ('fileTree' === $strKey) {
                        foreach ($varValue as $k => $v) {
                            $varValue[$k] = Dbafs::addResource($v)->uuid;
                        }
                    }

                    $varValue = serialize($varValue);
                }

                // Build the attributes based on the "eval" array
                $arrAttribs = $GLOBALS['TL_DCA'][$dc->table]['fields'][$strField]['eval'];

                $arrAttribs['id'] = $dc->field;
                $arrAttribs['name'] = $dc->field;
                $arrAttribs['value'] = $varValue;
                $arrAttribs['strTable'] = $dc->table;
                $arrAttribs['strField'] = $strField;
                $arrAttribs['activeRecord'] = $dc->activeRecord;

                /** @var FileTree|PageTree $objWidget */
                $objWidget = new $GLOBALS['BE_FFL'][$strKey]($arrAttribs);
                echo $objWidget->generate();
                exit;
                break;

            // Feature/unfeature an element
            case 'toggleFeatured':
                if (class_exists($dc->table, false)) {
                    $dca = new $dc->table();

                    if (method_exists($dca, 'toggleFeatured')) {
                        $dca->toggleFeatured(
                            Input::post('id'),
                            (1 === Input::post('state')) ? true : false
                        );
                    }
                }
                exit;
                break;

            // Toggle subpalettes
            case 'toggleSubpalette':
                System::importStatic('BackendUser', 'User');

                // Check whether the field is a selector field and allowed for regular users
                // (thanks to Fabian Mihailowitsch) (see #4427)
                if (!\is_array($GLOBALS['TL_DCA'][$dc->table]['palettes']['__selector__'])
                    || !\in_array(
                        Input::post('field'),
                        $GLOBALS['TL_DCA'][$dc->table]['palettes']['__selector__'],
                        true
                    )
                    || ($GLOBALS['TL_DCA'][$dc->table]['fields'][Input::post('field')]['exclude']
                        && !$this->User->hasAccess($dc->table.'::'.Input::post('field'), 'alexf'))
                ) {
                    $msg = 'Field "%s" is not an allowed selector field (possible SQL injection attempt)';
                    $Logger->log(
                        LogLevel::ERROR,
                        sprintf($msg, Input::post('field')),
                        ['contao' => new ContaoContext(__METHOD__, TL_ERROR)]
                    );

                    header('HTTP/1.1 400 Bad Request');
                    die('Bad Request');
                }

                if ($dc instanceof General) {
                    $environment = $dc->getEnvironment();

                    if ('editAll' === $environment->getInputProvider()->getParameter('act')) {
                        $this->strAjaxId = preg_replace(
                            '/.*_([0-9a-zA-Z]+)$/',
                            '$1',
                            Input::post('id')
                        );
                        $Database
                            ->prepare('UPDATE '.$dc->table.' SET '.Input::post('field')."='"
                                .((int) (1 === Input::post('state')) ? 1 : '')."' WHERE id=?")
                            ->execute($this->strAjaxId)
                        ;

                        if (Input::post('load')) {
                            echo $dc->editAll($this->strAjaxId, Input::post('id'));
                        }
                    } else {
                        $Database
                            ->prepare('UPDATE '.$dc->table.' SET '.Input::post('field')."='"
                                .((int) (1 === Input::post('state')) ? 1 : '')."' WHERE id=?")
                            ->execute($dc->id)
                        ;

                        if (Input::post('load')) {
                            $action = new Action(
                                $environment->getInputProvider()->getParameter('act') ?: 'showAll'
                            );
                            echo $environment->getController()->handle($action);
                        }
                    }
                }
                break;
            default:
                exit;
                break;
        }
    }
}
