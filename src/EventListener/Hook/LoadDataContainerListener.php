<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension contao-rms-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

namespace Srhinow\ContaoRmsBundle\EventListener\Hook;

use Contao\Environment;
use Contao\Input;
use Srhinow\ContaoRmsBundle\Helper\RmsHelper;

class LoadDataContainerListener
{
    public function onLoadDataContainer(string $table): void
    {
        if (TL_MODE === 'BE' && !Environment::get('isAjaxRequest')) {
            $this->changeDcaIfRmsProtected($table);
        }
    }

    protected function changeDcaIfRmsProtected(string $strTable): void
    {
        $RmsHelper = RmsHelper::getInstance();

        if ($strTable !== Input::get('table')) {
            return;
        }

        if (!\in_array($strTable, $GLOBALS['rms_extension']['tables'], true)) {
            return;
        }

        // Wenn es sich um eine Listenansicht unterhalb einer parent-Tabelle handelt
        // den Status anhand der Parent-Tabelle prüfen
        $testTable = $strTable;
        if ('edit' !== Input::get('act')
            && 'show' !== Input::get('act')
            && !empty($GLOBALS['TL_DCA'][$strTable]['config']['ptable'])
        ) {
            $testTable = $GLOBALS['TL_DCA'][$strTable]['config']['ptable'];
        }

        switch ($testTable) {
            case 'tl_content':
                $protected = $RmsHelper->isContentRmsProtected($testTable);
                break;
            default:
                $protected = $RmsHelper->isTableRmsProtected($testTable);
        }

        if ($RmsHelper->isMemberOfSlaves()
            && $protected
            && ($GLOBALS['TL_CONFIG']['rms_active'])
            || Input::get('author')) {
            $GLOBALS['TL_DCA'][$strTable]['config']['dataContainer'] = 'RmsTable';

            //falls keine separaten callbacks existieren die Standart-Callbacks aufrufen
            $GLOBALS['TL_DCA'][$strTable]['config']['onrestore_callback'][] = (method_exists(
                'srhinow.contao_rms_bundle.listener.dca.'.$strTable,
                'onRestoreCallback'
            ))
                ? ['srhinow.contao_rms_bundle.listener.dca.'.$strTable, 'onRestoreCallback']
                : ['srhinow.contao_rms_bundle.listener.dca.rms_defaults', 'onRestoreCallback'];

            $GLOBALS['TL_DCA'][$strTable]['config']['oncut_callback'][] = (method_exists(
                'srhinow.contao_rms_bundle.listener.dca.'.$strTable,
                'onCutCallback'
            ))
                ? ['srhinow.contao_rms_bundle.listener.dca.'.$strTable, 'onCutCallback']
                : ['srhinow.contao_rms_bundle.listener.dca.rms_defaults', 'onCutCallback'];

            if ('edit' === Input::get('act') || 'show' === Input::get('act')) {
                $GLOBALS['TL_DCA'][$strTable]['config']['onsubmit_callback'][] = (method_exists(
                    'srhinow.contao_rms_bundle.listener.dca.'.$strTable,
                    'onSubmitCallback'
                ))
                    ? ['srhinow.contao_rms_bundle.listener.dca.'.$strTable, 'onSubmitCallback']
                    : ['srhinow.contao_rms_bundle.listener.dca.rms_defaults', 'onSubmitCallback'];

                $GLOBALS['TL_DCA'][$strTable]['config']['getrms_callback'][] = (method_exists(
                    'srhinow.contao_rms_bundle.listener.dca.'.$strTable,
                    'onEditCallback'
                ))
                    ? ['srhinow.contao_rms_bundle.listener.dca.'.$strTable, 'onEditCallback']
                    : ['srhinow.contao_rms_bundle.listener.dca.rms_defaults', 'onEditCallback'];
            } else {
                $GLOBALS['TL_DCA'][$strTable]['config']['getrms_listview_callback'][] = (method_exists(
                    'srhinow.contao_rms_bundle.listener.dca.'.$strTable,
                    'onListCallback'
                ))
                    ? ['srhinow.contao_rms_bundle.listener.dca.'.$strTable, 'onListCallback']
                    : ['srhinow.contao_rms_bundle.listener.dca.rms_defaults', 'onListCallback'];
            }
        }

        $GLOBALS['TL_DCA'][$strTable]['config']['getrms_listview_callback'][] = (method_exists(
            'srhinow.contao_rms_bundle.listener.dca.'.$strTable,
            'onListCallback'
        ))
            ? ['srhinow.contao_rms_bundle.listener.dca.'.$strTable, 'onListCallback']
            : ['srhinow.contao_rms_bundle.listener.dca.rms_defaults', 'onListCallback'];

        if ($protected
            && ($GLOBALS['TL_CONFIG']['rms_active'])
            || \Input::get('author')) {
            $GLOBALS['TL_DCA'][$strTable]['config']['ondelete_callback'][] = (method_exists(
                'srhinow.contao_rms_bundle.listener.dca.'.$strTable,
                'onDeleteCallback'
            ))
                ? ['srhinow.contao_rms_bundle.listener.dca.'.$strTable, 'onDeleteCallback']
                : ['srhinow.contao_rms_bundle.listener.dca.rms_defaults', 'onDeleteCallback'];
        }
    }
}
