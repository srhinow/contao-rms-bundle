<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension contao-rms-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

namespace Srhinow\ContaoRmsBundle\EventListener\Dca;

use Contao\Backend;
use Contao\BackendTemplate;
use Contao\DataContainer;
use Contao\Image;
use Contao\Input;
use Contao\StringUtil;
use Srhinow\ContaoRmsBundle\Model\RmsLogModel;
use Srhinow\ContaoRmsBundle\Versions\RmsVersions;

class RmsLog extends Backend
{
    /**
     * Import the back end user object.
     */
    public function __construct()
    {
        parent::__construct();
        $this->import('BackendUser', 'User');
    }

    public function onLoadCallback(DataContainer $dc): void
    {
        $this->fillEmptyDiffField();
    }

    /**
     * show diff between preview and active version.
     *
     * @param array
     * @param string
     * @param string
     * @param string
     * @param string
     * @param string
     *
     * @return string
     */
    public function showDiff($row, $href, $label, $title, $icon, $attributes)
    {
        if ('show_diff' === Input::get('key') && $row['ref_id'] === Input::get('ref_id')) {
            $objVersions = new RmsVersions($row['ref_table']);

            if (null === $row['diff']) {
                $arrFrom = StringUtil::deserialize($row['data_old']);
                $arrTo = StringUtil::deserialize($row['data_new']);
                $objVersions->compare($arrFrom, $arrTo);
            } else {
                $arrDiff = StringUtil::deserialize($row['diff']);
                $objVersions->setArrCompare($arrDiff);
            }

            $objVersions->setFirstSave($row['rms_first_save']);
            $objVersions->generateOutput();
        }

        $objTemplate = new BackendTemplate('be_list_button');
        $objTemplate->url = $this->addToUrl($href.'&ref_id='.$row['ref_id']);
        $objTemplate->title = StringUtil::specialchars($title);
        $objTemplate->attributes = $attributes;
        $objTemplate->image = Image::getHtml($icon, $label);

        return  $objTemplate->parse();
    }

    public function fillEmptyDiffField(): void
    {
        if (null === ($objRmsLogs = RmsLogModel::findBy(['diff is null'], null))) {
            return;
        }

        while ($objRmsLogs->next()) {
            $objRmsLog = $objRmsLogs->current();

            $objVersions = new RmsVersions($objRmsLog->ref_table);
            $arrFrom = StringUtil::deserialize($objRmsLog->data_old);
            $arrTo = StringUtil::deserialize($objRmsLog->data_new);
            $objVersions->compare($arrFrom, $arrTo);

            $objRmsLog->diff = $objVersions->getArrCompare();
            $objRmsLog->save();
        }
    }
}
