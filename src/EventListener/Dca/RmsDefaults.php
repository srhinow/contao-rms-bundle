<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension contao-rms-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

namespace Srhinow\ContaoRmsBundle\EventListener\Dca;

use Contao\Backend;
use Contao\Controller;
use Contao\CoreBundle\Monolog\ContaoContext;
use Contao\DataContainer;
use Contao\Input;
use Contao\System;
use Srhinow\ContaoRmsBundle\Helper\RmsHelper;
use Srhinow\ContaoRmsBundle\Model\RmsModel;
use Srhinow\ContaoRmsBundle\Model\RmsTmpModel;

class RmsDefaults extends Backend
{
    protected $RmsHelper;

    protected $settings = [];

    /**
     * Load the database object.
     */
    public function __construct()
    {
        parent::__construct();
        $this->RmsHelper = RmsHelper::getInstance();
        $this->settings = $this->RmsHelper->getSettings();
        $this->import('BackendUser', 'User');
    }

    /**
     * overwrite table-data if current BackendUser a low-level-redakteur.
     *
     * @param object
     *
     * @return string or object
     */
    public function onListCallback(DataContainer $dc, $rowArr)
    {
        //nötige Paramter ermitteln

        $strTable = (Input::get('table')) ? Input::get('table') : 'tl_'.Input::get('do');
        $contentId = $rowArr['id'];

        //wenn eins der nötigen Parameter fehlt -> hier abbrechen
        if (!$strTable || !$contentId) {
            return $rowArr;
        }

        $RmsHelper = RmsHelper::getInstance();
        //hole nicht freigegebene Daten von dem Redakteur fuer diesen Content
        if($RmsHelper->isMemberOfSlaves()) {
            $userId = (Input::get('author')) ? Input::get('author') : $this->User->id;
            $objStoredData = RmsModel::findRefByUser($strTable, $contentId, $userId);
        } else  {
            $objStoredData = RmsModel::findRef($strTable, $contentId);
        }


        if (null === $objStoredData) {
            return $rowArr;
        }

        //wenn bereits eine nicht freigegebene Bearbeitung vorliegt
        return unserialize($objStoredData->data);
    }

    /**
     * overwrite table-data and backup in tmp-table if current BackendUser a low-level-redakteur.
     *
     * @param object
     * @param object
     */
    public function onEditCallback(DataContainer $dc, $liveDataObj)
    {
        $liveDataArr = $liveDataObj->fetchAssoc();

        //nötige Parameter ermitteln
        $userId = (Input::get('author')) ? Input::get('author') : $this->User->id;
        $strTable = (Input::get('table')) ? Input::get('table') : 'tl_'.Input::get('do');
        $contentId = Input::get('id');

        //wenn eins der nötigen Parameter fehlt -> hier abbrechen
        if (!$userId || !$strTable || !$contentId) {
            return $liveDataArr;
        }

        // falls es eine tl_content ist und der Datentyp ignoriert werden soll -> hier abbrechen
        $ignoreTypedArr = array_map('trim', explode(',', $this->settings['ignore_content_types']));

        if ('tl_content' === $strTable
            && \is_array($ignoreTypedArr)
            && \in_array($liveDataArr['type'], $ignoreTypedArr, true)) {
            return $liveDataArr;
        }

        //loesche evtl alte Datensätze zu diesem Element aus der tl_rms_tmp
        $this->Database
            ->prepare('DELETE FROM tl_rms_tmp WHERE ref_id=? AND ref_table=? AND ref_author=?')
            ->execute(
                $contentId,
                $strTable,
                $userId
            )
        ;

        //loesche alle Datensätze die älter als einen Monat sind aus der tl_rms_tmp
        $this->Database
            ->prepare('DELETE FROM tl_rms_tmp WHERE tstamp <= ?')
            ->execute(strtotime('-1 Month'))
        ;

        //sichere live-daten
        $objRmsTmp = new RmsTmpModel();
        $objRmsTmp->data = serialize($liveDataArr);
        $objRmsTmp->ref_id = $contentId;
        $objRmsTmp->ref_table = $strTable;
        $objRmsTmp->ref_author = $userId;
        $objRmsTmp->tstamp = time();
        $objRmsTmp->save();

        //hole nicht freigegebene Daten von dem Redakteur fuer diesen Content
        if (null === ($objRms = RmsModel::findRefByUser($strTable, $contentId, $userId))) {
            return $liveDataArr;
        }

        //wenn bereits eine nicht freigegebene Bearbeitung vorliegt
        $arrRmsData = unserialize($objRms->data);

        $arrOverwrite = $liveDataArr;
        foreach ($arrRmsData as $field => $value) {
            if (null === $value) {
                continue;
            }
            $arrOverwrite[$field] = $value;
        }

        return $arrOverwrite;
    }

    /**
     * set or update a entry in rms-table.
     *
     * @param object
     */
    public function onSubmitCallback(DataContainer $dc): void
    {
        $this->import('BackendUser');
        $this->import('srhinow.contao_rms_bundle.helper.rms_helper', 'rmsHelper');
        $settings = $this->rmsHelper->getSettings();

        $userId = (Input::get('author')) ? Input::get('author') : $this->BackendUser->id;
        $strTable = (Input::get('table')) ? Input::get('table') : 'tl_'.Input::get('do');
        $intId = Input::get('id');
        Controller::loadDataContainer($strTable);

        if (!$userId || !$strTable || !$intId) {
            return;
        }
        if (Input::post('type') !== $dc->activeRecord->type) {
            $dc->activeRecord->type = Input::post('type');
        }

        // falls es eine tl_content ist und der Datentyp ignoriert werden soll -> hier abbrechen
        $ignoreTypedArr = array_map('trim', explode(',', $settings['ignore_content_types']));
        if ('tl_content' === $strTable
            && \is_array($ignoreTypedArr)
            && \in_array($dc->activeRecord->type, $ignoreTypedArr, true)) {
            return;
        }

        // Get the currently available fields
        $arrFields = array_flip($this->Database->getFieldnames($strTable));

        //create db-field-array with new data
        foreach ($arrFields as $fieldName => $colNum) {
            if (\in_array($fieldName, ['PRIMARY', 'INDEX'], true)) {
                continue;
            }

            $fieldEval = $GLOBALS['TL_DCA'][$strTable]['fields'][$fieldName]['eval'];

            if (null !== Input::post($fieldName)) {
                $value = (!empty($fieldEval['rte']) || $fieldEval['decodeEntities'])
                    ? html_entity_decode(Input::post($fieldName))
                    : Input::post($fieldName);

                $newData[$fieldName] = ($fieldEval['multiple'])
                    ? explode(',',$value)
                    : $value;
            }
        }

        if ('tl_news' === $strTable) {
            $newData['date'] = strtotime(
                date('Y-m-d', $dc->activeRecord->date)
                .' '
                .date('H:i:s', $dc->activeRecord->time)
            );
            $newData['time'] = $newData['date'];
        }

        //hole gesicherte und freigegebene Daten von dem Redakteur für diesen Content
        $objRmsTmp = RmsTmpModel::findRefByUser($strTable, $intId, $userId);

        //wenn z.B. der Datensatz neu angelegt wurde
        if (null !== $objRmsTmp) {
            $data = unserialize($objRmsTmp->data);
        } else {
            $data = $newData;
        }

        // create / first-save
        $isNewEntryObj = $this->Database
            ->prepare('SELECT count(*) c FROM `'.$strTable.'` WHERE `id`=? AND `tstamp`=?')
            ->limit(1)
            ->execute($intId, 0)
        ;

        if (1 === (int) $isNewEntryObj->c) {
            $data['tstamp'] = time();
            $data['rms_first_save'] = 1;
        }

        //overwrite with live-data
        $data['rms_new_edit'] = 1;
        $data['rms_ref_table'] = $strTable;
        $data['rms_notice'] = $newData['rms_notice'];
        if($this->Database->fieldExists('alias',$strTable)) {

            $data['alias'] = $newData['alias'];
        }
        $this->Database->prepare('UPDATE '.$strTable.' %s WHERE id=?')->set($data)->execute($intId);

        //status
        if ($_SESSION['send_rms_info']) {
            $status = ($this->rmsHelper->isMemberOfMasters()) ? 1 : 0; // '1'=>'zurückgegeben', '0'=>'Zur Freigabe'
        } else {
            $status = ($this->rmsHelper->isMemberOfMasters()) ? 1 : 2; // '1'=>'zurückgegeben', '2'=>'In Bearbeitung'
        }

        //overwrite with new-data
        $newRmsData = array_merge($data, $newData);

        // create an BE-URL-String to edit
        $getParamArr = ['do', 'table', 'id', 'act'];
        $urlParams = [];
        foreach ($getParamArr as $param) {
            if (null !== Input::get($param)) {
                $urlParams[] = $param.'='.Input::get($param);
            }
        }

        // get root-parent-table
        $pTable = ('tl_content' === $strTable)
            ? $dc->activeRecord->ptable
            : $GLOBALS['TL_DCA'][$strTable]['config']['ptable'];

        $rootPTable = $this->rmsHelper->getRootParentTable($strTable, $pTable);

        // hole die email und vorschau-url
        $sectionSettings = $this->rmsHelper->getRmsSectionSettings($intId, $strTable, $pTable);

//        $logger = System::getContainer()->get('monolog.logger.contao');
//
//        $logger->log(
//            100,
//            json_encode($newRmsData),
//            ['contao' => new ContaoContext(__METHOD__, 'GENERAL')]
//        );

        $arrRmsData = [
            'tstamp' => time(),
            'ref_id' => $intId,
            'ref_table' => $strTable,
            'ref_author' => $userId,
            'ref_notice' => $newRmsData['rms_notice'],
            'do' => Input::get('do'),
            'edit_url' => implode('&', $urlParams),
            'root_ptable' => $rootPTable,
            'master_id' => $sectionSettings['master_id'],
            'master_email' => $sectionSettings['master_email'],
            'preview_jumpTo' => $sectionSettings['preview_jumpTo'],
            'status' => $status,
            'data' => $newRmsData,
        ];

        // existiert schon eine Bearbeitung?
        $objRms = RmsModel::findRefByUser($strTable, $intId, $userId);

        if (null === $objRms) {
            $objRms = new RmsModel();
        }

        // damit sie als Modifiziert gelten
        foreach ($arrRmsData as $field => $value) {
            $objRms->{$field} = $value;
        }

        // Aenderungen in der tl_rms speichern
        $objRms->save();
    }

    /**
     * oncut.
     */
    public function onCutCallback(\DataContainer $dc): void
    {
        //ToDo: da der Inhalt ja eventuell schon freigegeben wurde und auch plötzlich nicht verschwinden
        // darf nur weil es an einer anderen Stelle stehen soll, ist der Umgang als Freigabe-Schutz noch unklar.
    }

    /**
     * delete from rms-table when item delete.
     *
     * @var object
     */
    public function onDeleteCallback(\DataContainer $dc): void
    {
        $strTable = (\Input::get('table')) ? \Input::get('table') : 'tl_'.$this->Input->get('do');
        $intId = \Input::get('id');

        $this->Database->prepare('DELETE FROM tl_rms WHERE ref_id=? AND ref_table=?')
            ->execute(
                $intId,
                $strTable
            )
        ;

        //bei neu angelegte Inhalte die origninalen Inhalte auch löschen. #8)
        $this->Database->prepare('DELETE FROM '.$strTable.' WHERE `id` = ? AND `rms_first_save`=?')
            ->limit(1)
            ->execute($intId, 1)
        ;
    }

    /**
     * overwrite only rms-date und reset live-data.
     *
     * @param int
     * @param string
     * @param array
     * @param int
     */
    public function onRestoreCallback($intPid, $strTable, $data, $intVersion): void
    {
        $this->import('srhinow.contao_rms_bundle.helper.rms_helper', 'rmsHelper');

        if ($this->rmsHelper->isMemberOfMasters() && Input::get('author')) {
            //hole letzte Livedaten
            $objRmsTmp = RmsTmpModel::findRefByUser($strTable, $intPid, Input::get('author'));

            $liveData = (null !== $objRmsTmp) ? unserialize($objRmsTmp->data) : $data;

            //ueberschreibe wieder die Livedaten
            $this->Database->prepare('UPDATE '.$strTable.' %s  WHERE id=?')
                ->set($liveData)
                ->execute($intPid)
            ;

            //ersetze frei-zugebenedes Release mit Versions-Daten
            $objRms = RmsModel::findRefByUser($strTable, $intPid, Input::get('author'));
            $objRms->data = $data;
            $objRms->save();
        }
    }
}
