<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension contao-rms-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

namespace Srhinow\ContaoRmsBundle\EventListener\Dca;

use Contao\Backend;
use Contao\Date;
use Contao\Input;
use Contao\UserModel;

class Faq extends Backend
{
    /**
     * Import the back end user object.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * add RMS-Fields in menny content-elements (DCA).
     *
     * @var object
     */
    public function addRmsFields(\DataContainer $dc): void
    {
        $strTable = Input::get('table');

        //defined blacklist palettes
        $rm_palettes_blacklist = ['__selector__'];

        //add Field in meny content-elements
        foreach ($GLOBALS['TL_DCA'][$strTable]['palettes'] as $name => $field) {
            if (\in_array($name, $rm_palettes_blacklist, true)) {
                continue;
            }

            $GLOBALS['TL_DCA'][$strTable]['palettes'][$name] .= ';{rms_legend:hide},rms_notice,rms_release_info';
        }
    }

    /**
     * custom modify the rms-Preview
     * used from rmsHelper->modifyForPreview() -> is a parseTemplate->HOOK.
     *
     * @param object
     * @param array
     *
     * @return object
     */
    public function modifyForPreview($templObj, $newArr)
    {
        if (\is_array($newArr) && \count($newArr) > 0) {
            foreach ($newArr as $k => $v) {
                $templObj->$k = $v;
            }

            //author
            $objAuthor = UserModel::findByPk($templObj->author);
            $this->Template->info = sprintf(
                $GLOBALS['TL_LANG']['MSC']['faqCreatedBy'],
                Date::parse($templObj->dateFormat, $templObj->tstamp),
                $objAuthor->name
            );
        }

        return $templObj;
    }
}
