<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension contao-rms-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

namespace Srhinow\ContaoRmsBundle\EventListener\Dca;

use Contao\Backend;
use Contao\BackendTemplate;
use Contao\Controller;
use Contao\DataContainer;
use Contao\Image;
use Contao\Input;
use Contao\Message;
use Contao\Model;
use Contao\StringUtil;
use Contao\UserModel;
use Contao\Validator;
use Srhinow\ContaoRmsBundle\Model\RmsModel;
use Srhinow\ContaoRmsBundle\Model\RmsTmpModel;
use Srhinow\ContaoRmsBundle\Service\Log\RmsLog;
use Srhinow\ContaoRmsBundle\Versions\RmsVersions;

class Rms extends Backend
{
    /**
     * Import the back end user object.
     */
    public function __construct()
    {
        parent::__construct();
        $this->import('BackendUser', 'User');
        $this->import('Database');
    }

    /**
     * list only Entries for the cureent master-member.
     */
    public function filterMemberEntries(): void
    {
        if (!$this->User->isAdmin) {
            $GLOBALS['TL_DCA']['tl_rms']['list']['sorting']['filter'] = [
                ['master_id=?', $this->User->id],
            ];
        }
    }

    /**
     * List a recipient.
     *
     * @param array
     *
     * @return string
     */
    public function listRecipient($row)
    {
        //get user
        $objUser = UserModel::findByPk($row['ref_author']);

        //get referenz
        $strModelClass = Model::getClassFromTable($row['ref_table']);
        if (!class_exists($strModelClass)) {
            return '';
        }

        /** @var \Model $strModelClass */
        $objTargetModel = $strModelClass::findByPk($row['ref_id']);
        if (null === $objTargetModel) {
            return '';
        }

        $objTemplate = new BackendTemplate('be_rms_list');
        $objTemplate->langRms = $GLOBALS['TL_LANG']['tl_rms'];
        $objTemplate->user = $objUser->row();
        $objTemplate->row = $row;
        $objTemplate->isFirstSave = (1 === $objTargetModel->rms_first_save) ? true : false;

        return $objTemplate->parse()."\n";
    }

    /**
     * Return the edit article button.
     *
     * @param array
     * @param string
     * @param string
     * @param string
     * @param string
     * @param string
     *
     * @return string
     */
    public function editElement($row, $href, $label, $title, $icon, $attributes)
    {
        $objTemplate = new BackendTemplate('be_list_button');
        $objTemplate->url = $this->addToUrl($row['edit_url'].'&author='.$row['ref_author']);
        $objTemplate->title = StringUtil::specialchars($title);
        $objTemplate->attributes = $attributes;
        $objTemplate->image = Image::getHtml($icon, $label);

        return  $objTemplate->parse();
    }

    /**
     * setzt das orignale Elment wieder als unbearbeitet und löscht die bearbeiteten-Datensätze.
     */
    public function setUnEditData(DataContainer $dc): void
    {
        $refTable = $dc->activeRecord->ref_table;
        $refId = $dc->activeRecord->ref_id;

        // count other edits for release
        $countRms = RmsModel::countBy(['id=?'], [$dc->id]);

        if (0 < $countRms) {
            //get referenz
            $strModelClass = Model::getClassFromTable($refTable);
            if (!class_exists($strModelClass)) {
                return;
            }

            /** @var Model $strModelClass */
            $objTargetModel = $strModelClass::findByPk($refId);
            if (null === $objTargetModel) {
                return;
            }

            //alle rms-Felder in geänderter Tabelle zurück setzen
            $objTargetModel->rms_release_info = '';
            $objTargetModel->rms_notice = '';
            $objTargetModel->rms_ref_table = '';
            $objTargetModel->rms_new_edit = '';
            $objTargetModel->rms_first_save = '';
            $objTargetModel->save();

            $changeDeleted = false;
            if (null !== ($objRms = RmsModel::findByPk($dc->id))) {
                $objRms->delete();
                $changeDeleted = true;
            }

            if (null !== ($objRmsTmpCollection = RmsTmpModel::findBy(['ref_id=?', 'ref_table=?'], [$refId, $refTable]))) {
                //delete new empty elements
                while ($objRmsTmpCollection->next()) {
                    $objRmsTmpCollection->delete();
                }
            }

            if (\is_array($GLOBALS['TL_HOOKS']['rmsRejectEdit'])) {
                foreach ($GLOBALS['TL_HOOKS']['rmsRejectEdit'] as $callback) {
                    if (\is_array($callback)) {
                        $this->import($callback[0]);
                        $this->{$callback[0]}->$callback[1]($refTable, $refId, $changeDeleted);
                    } elseif (\is_callable($callback)) {
                        $callback($this, false);
                    }
                }
            }

            if ($changeDeleted) {
                $msg = sprintf(
                    'Die Änderungen vom Datensatz %s (Tabelle: %s) wurden gelöscht.',
                    $refId,
                    $refTable
                );
                Message::addConfirmation($msg);
            } else {
                $msg = sprintf(
                    'Die Änderungen vom Datensatz %s (Tabelle: %s) konnten nicht gelöscht werden. ',
                    $refId,
                    $refTable
                );
                Message::addError($msg);
            }
        }
    }

    /**
     * show diff between preview and active version.
     *
     * @param array
     * @param string
     * @param string
     * @param string
     * @param string
     * @param string
     *
     * @return string
     */
    public function showDiff($row, $href, $label, $title, $icon, $attributes)
    {
        if (
            'show_diff' === Input::get('key')
            && $row['ref_id'] === Input::get('ref_id')) {
            $objVersions = new RmsVersions($row['ref_table']);

            if (!Input::get('ref_id')) {
                $strBuffer = '<p>Es wurde keine ref_id &uuml;bergeben.</p>';
                $objVersions->createOutput($strBuffer);
            }

            if (!$row['ref_table']) {
                $strBuffer = '<p>Es wurde keine ref_table &uuml;bergeben.</p>';
                $objVersions->createOutput($strBuffer);
            }

            //get referenz
            $strModelClass = Model::getClassFromTable($row['ref_table']);
            if (!class_exists($strModelClass)) {
                $strBuffer = '<p>Es konnte keine Modelklasse zu '.$row['ref_table'].' gefunden werden.</p>';
                $objVersions->createOutput($strBuffer);
            }

            /** @var \Model $strModelClass */
            $objReference = $strModelClass::findByPk(Input::get('ref_id'));
            if (null === $objReference) {
                $strBuffer = '<p>There are no reference of '.$row['ref_table'].'.id='.\Input::get('ref_id').'</p>';
                $objVersions->createOutput($strBuffer);
            }

            $from = $objReference->row();
            $to = StringUtil::deserialize($row['data']);

            $objVersions->compare($from, $to);
            $objVersions->setFirstSave($row['rms_first_save']);
            $objVersions->generateOutput();
        }

        $objTemplate = new BackendTemplate('be_list_button');
        $objTemplate->url = $this->addToUrl($href.'&ref_id='.$row['ref_id']);
        $objTemplate->title = StringUtil::specialchars($title);
        $objTemplate->attributes = $attributes;
        $objTemplate->image = Image::getHtml($icon, $label);

        return  $objTemplate->parse();
    }

    /**
     * overwrite the old entry if entry acknowledge.
     *
     * @var object
     */
    public function acknowledgeEntry(DataContainer $dc): void
    {
        if (null === ($objRms = RmsModel::findByPk($dc->id))) {
            return;
        }

        $strModelClass = Model::getClassFromTable($objRms->ref_table);
        if (!class_exists($strModelClass)) {
            return;
        }

        /** @var \Model $strModelClass */
        if (null === ($objTargetModel = $strModelClass::findByPk($objRms->ref_id))) {
            return;
        }

        // Log-Eintrag setzen
        RmsLog::newEntry($objRms);

        $arrNewData = unserialize($objRms->data);
        if (!\is_array($arrNewData) || \count($arrNewData) < 1) {
            return;
        }

        // Feldwerte aus Änderungsdatensatz auf originalen Datensatz-Eintrag anweden.
        Controller::loadDataContainer($objRms->ref_table);
        foreach ($arrNewData as $field => $value) {
            $fieldDca = $GLOBALS['TL_DCA'][$objRms->ref_table]['fields'][$field];

            switch ($fieldDca['eval']['rgxp']) {
                case 'date':
                case 'time':
                case 'datim':
                    if(is_string($value) && $value !== '') {
                        $value = (int) $value;
                    }
                    break;
            }

            if (Validator::isStringUuid($value))
            {
                $value = StringUtil::uuidToBin($value);
            }

            $objTargetModel->{$field} = $value;
        }

        $objTargetModel->rms_notice = '';
        $objTargetModel->rms_release_info = '';
        $objTargetModel->rms_first_save = '';
        $objTargetModel->rms_new_edit = '';
        $objTargetModel->tstamp = time();

        //correct any fields
        switch ($objRms->ref_table) {
            case 'tl_calendar_events':
            case 'tl_news':
                $objTargetModel->published = 1;
                break;
            case 'tl_content':
                $objTargetModel->invisible = '';
                break;
            default:
                break;
        }

        if (\is_array($GLOBALS['TL_HOOKS']['RmsPublish'])) {
            foreach ($GLOBALS['TL_HOOKS']['RmsPublish'] as $callback) {
                $this->import($callback[0]);
                $this->{$callback[0]}->{$callback[1]}($objRms->ref_table, $arrNewData, $objTargetModel);
            }
        }

        // die geänderten Daten auf den jeweiligen Datensatz anwenden
        $objTargetModel->save();

        // evl. Hooks nach dem Aktualisieren des Datensatzes abarbeiten
        if (\is_array($GLOBALS['TL_HOOKS']['rmsPublishAfter'])) {
            foreach ($GLOBALS['TL_HOOKS']['rmsPublishAfter'] as $callback) {
                $this->import($callback[0]);
                $this->{$callback[0]}->{$callback[1]}($objRms->ref_table, $arrNewData, $objTargetModel);
            }
        }

        $msg = sprintf(
            $GLOBALS['TL_LANG']['MSC']['rms_succesfully_live'],
            $objRms->ref_table.' (ID: '.$objTargetModel->id.')'
        );

        if (null !== ($objRmsTmpCollection = RmsTmpModel::findBy(
            ['ref_id=?', 'ref_table=?'],
            [$objRms->ref_id, $objRms->ref_table]))
        ) {
            // Einträge zu dem Element aus tl_rms_tmp löschen
            while ($objRmsTmpCollection->next()) {
                $objRmsTmpCollection->delete();
            }
        }

        // Änderungseintrag aus tl_rms löschen
        $objRms->delete();

        Message::addConfirmation($msg);

        $this->redirect(str_replace('&key=acknowledge', '', $this->Environment->request));
    }
}
