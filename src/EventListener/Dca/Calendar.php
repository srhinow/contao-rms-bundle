<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension contao-rms-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

namespace Srhinow\ContaoRmsBundle\EventListener\Dca;

use Contao\Backend;
use Contao\DataContainer;

class Calendar extends Backend
{
    /**
     * Import the back end user object.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * add RMS-Fields in enny news archive palettes (DCA).
     *
     * @var object
     */
    public function addRmsFields(DataContainer $dc): void
    {
        //defined blacklist palettes
        $rm_palettes_blacklist = ['__selector__'];

        //add Field in meny content-elements
        foreach ($GLOBALS['TL_DCA']['tl_calendar']['palettes'] as $name => $field) {
            if (\in_array($name, $rm_palettes_blacklist, true)) {
                continue;
            }

            $GLOBALS['TL_DCA']['tl_calendar']['palettes'][$name] .= '
                ;{rms_settings_legend:hide},rms_protected
                ;{rms_legend:hide},rms_notice,rms_release_info
            ';
        }
    }
}
