<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension contao-rms-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

namespace Srhinow\ContaoRmsBundle\EventListener\Dca;

use Contao\Backend;
use Contao\CoreBundle\Monolog\ContaoContext;
use Contao\Database;
use Contao\DataContainer;
use Contao\Input;
use Contao\PageModel;
use Contao\System;
use Psr\Log\LogLevel;
use Srhinow\ContaoRmsBundle\Model\RmsSettingsModel;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class RmsSettings extends Backend
{
    /**
     * @var object|null
     */
    private $logger;

    /**
     * Import the back end user object.
     */
    public function __construct()
    {
        parent::__construct();
        $this->import('BackendUser', 'User');
        $this->logger = System::getContainer()->get('monolog.logger.contao');
    }

    /**
     * Check permissions to edit table tl_rms_settings.
     */
    public function checkPermission(): void
    {
        if ($this->User->isAdmin) {
            return;
        }

        // Set root IDs
        if (!\is_array($this->User->calendars) || empty($this->User->calendars)) {
            $root = [0];
        } else {
            $root = $this->User->calendars;
        }

        $id = (null !== Input::get('id')) ? Input::get('id') : CURRENT_ID;

        // Check current action
        switch (Input::get('act')) {
            case 'create':
                if (null === Input::get('pid') || !\in_array(Input::get('pid'), $root, true)) {
                    $msg = 'Not enough permissions to create Event Reservation in channel ID "%s"';
                    $this->logger->log(
                        LogLevel::ERROR,
                        sprintf($msg, Input::get('pid')),
                        ['contao' => new ContaoContext(__METHOD__, 'ERROR')]
                    );
                    $this->redirect('contao/main.php?act=error');
                }
                break;

            case 'edit':
            case 'show':
            case 'copy':
            case 'delete':
            case 'toggle':
                $objSettings = RmsSettingsModel::findByPk($id);

                if (null === $objSettings) {
                    $msg = 'Invalid Rms-Settings ID "%s"';
                    $this->logger->log(
                        LogLevel::ERROR,
                        sprintf($msg, $id),
                        ['contao' => new ContaoContext(__METHOD__, 'ERROR')]
                    );
                    $this->redirect('contao/main.php?act=error');
                }

                if (!\in_array($objSettings->pid, $root, true)) {
                    $msg = 'Not enough permissions to %s rms_settings ID "%s"';
                    $this->logger->log(
                        LogLevel::ERROR,
                        sprintf($msg, Input::get('act'), $id),
                        ['contao' => new ContaoContext(__METHOD__, 'ERROR')]
                    );
                    $this->redirect('contao/main.php?act=error');
                }
                break;

            case 'select':
            case 'editAll':
            case 'deleteAll':
            case 'overrideAll':
                if (!\in_array($id, $root, true)) {
                    $msg = 'Not enough permissions to access rms_setting ID "%s"';
                    $this->logger->log(
                        LogLevel::ERROR,
                        sprintf($msg, $id),
                        ['contao' => new ContaoContext(__METHOD__, 'ERROR')]
                    );
                    $this->redirect('contao/main.php?act=error');
                }

                $objSettings = RmsSettingsModel::findByPk($id);

                if (null === $objSettings) {
                    $msg = 'Invalid RMS Setting ID "%s"';
                    $this->logger->log(
                        LogLevel::ERROR,
                        sprintf($msg, $id),
                        ['contao' => new ContaoContext(__METHOD__, 'ERROR')]
                    );
                    $this->redirect('contao/main.php?act=error');
                }

                /** @var SessionInterface $objSession */
                $objSession = System::getContainer()->get('session');

                $session = $objSession->all();
                $session['CURRENT']['IDS'] = array_intersect((array) $session['CURRENT']['IDS'], [$objSettings->id]);
                $objSession->replace($session);

                break;

            default:
                if (Input::get('act')) {
                    $msg = 'Invalid command "%s"';
                    $this->logger->log(
                        LogLevel::ERROR,
                        sprintf($msg, Input::get('act')),
                        ['contao' => new ContaoContext(__METHOD__, 'ERROR')]
                    );
                    $this->redirect('contao/main.php?act=error');
                } elseif (!\in_array($id, $root, true)) {
                    $msg = 'Not enough permissions to access rms_settings ID "%s"';
                    $this->logger->log(
                        LogLevel::ERROR,
                        sprintf($msg, $id),
                        ['contao' => new ContaoContext(__METHOD__, 'ERROR')]
                    );
                    $this->redirect('contao/main.php?act=error');
                }
                break;
        }
    }

    /**
     * Get all modules and return them as array.
     *
     * @return array
     */
    public function getModules()
    {
        $arrModules = [];
        $objModules = $this->Database->execute('SELECT m.id, m.name, t.name AS theme FROM tl_module m 
    LEFT JOIN tl_theme t ON m.pid=t.id ORDER BY t.name, m.name');

        while ($objModules->next()) {
            $arrModules[$objModules->theme][$objModules->id] = $objModules->name.' (ID '.$objModules->id.')';
        }

        return $arrModules;
    }

    /**
     * create an entry if id=1 not exists.
     */
    public function createPropertyEntry(): void
    {
        $objSettings = RmsSettingsModel::findByPk($GLOBALS['RMS']['SETTINGS_ID']);

        if (null === $objSettings) {
            $objSettings = new RmsSettingsModel();
            $objSettings->id = $GLOBALS['RMS']['SETTINGS_ID'];
            $objSettings->save();
        }
    }

    /**
     * Get all root-Pages and return them as array.
     *
     * @param DataContainer
     *
     * @return array
     */
    public function getRootPages(DataContainer $dc)
    {
        $rootArr = [];
        $objRootPage = PageModel::findBy(['type=?'], ['root']);
        if (null === $objRootPage) {
            return $rootArr;
        }

        while ($objRootPage->next()) {
            $rootArr[$objRootPage->id] = $objRootPage->title;
        }

        return $rootArr;
    }

    /**
     * Return available rms tables.
     *
     * @return array Array of tag tables
     */
    public function getReleaseTables()
    {
        $tables = [];
        foreach ($GLOBALS['rms_extension']['tables'] as $sourcetable) {
            $tables[$sourcetable] = $sourcetable;
        }

        return $tables;
    }

    public function getUserFromControlGroup(DataContainer $dc)
    {
        $arrReturn = [];
        if (!$dc->activeRecord->control_group) {
            return $arrReturn;
        }

        $DB = Database::getInstance();
        $objRelationUser = $DB
            ->prepare('SELECT `u`.* FROM `tl_user` `u` 
        LEFT JOIN `tl_user_to_group` `t` ON `u`.`id` = `t`.`user_id` WHERE `t`.`group_id`=?')
            ->execute($dc->activeRecord->control_group)
        ;
        if ($objRelationUser->numRows < 1) {
            return $arrReturn;
        }

        while ($objRelationUser->next()) {
            $arrReturn[$objRelationUser->id] = $objRelationUser->name.' ('.$objRelationUser->email.')';
        }

        return $arrReturn;
    }
}
