<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension contao-rms-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

namespace Srhinow\ContaoRmsBundle\EventListener\Dca;

use Contao\Backend;
use Contao\CoreBundle\Exception\AccessDeniedException;
use Contao\DataContainer;
use Contao\Image;
use Contao\Input;
use Contao\NewsModel;
use Contao\StringUtil;
use Contao\System;
use Contao\UserModel;
use Contao\Versions;
use Srhinow\ContaoRmsBundle\Helper\RmsHelper;
use Srhinow\ContaoRmsBundle\Model\RmsModel;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class News extends Backend
{
    /**
     * @var object|null
     */
    protected $logger;

    /**
     * News constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->import('BackendUser', 'User');
        $this->import('Database');
        $this->logger = System::getContainer()->get('monolog.logger.contao');
    }

    /**
     * Return the "toggle-button".
     *
     * @param array
     * @param string
     * @param string
     * @param string
     * @param string
     * @param string
     *
     * @return string
     */
    public function toggleIcon($row, $href, $label, $title, $icon, $attributes)
    {
        //test rms
        $objRms = RmsModel::findRef('tl_news', $row['id']);
        if (null === $objRms) {
            return'';
        }

        if (Input::get('tid')) {
            $this->toggleVisibility(Input::get('tid'), (1 === Input::get('state')));
            $this->redirect($this->getReferer());
        }

        // Check permissions AFTER checking the tid, so hacking attempts are logged
        if (!$this->User->isAdmin && !$this->User->hasAccess('tl_news::published', 'alexf')) {
            return '';
        }

        $href .= '&amp;tid='.$row['id'].'&amp;state='.($row['published'] ? '' : 1);

        if (!$row['published']) {
            $icon = 'invisible.gif';
        }

        return '<a href="'.$this->addToUrl($href).'" title="'.StringUtil::specialchars($title).'" '.$attributes.'>'
            .Image::getHtml($icon, $label).'</a> ';
    }

    /**
     * Disable/enable a user group.
     *
     * @param int           $intId
     * @param bool          $blnVisible
     * @param DataContainer $dc
     */
    public function toggleVisibility($intId, $blnVisible, DataContainer $dc = null): void
    {
        // Set the ID and action
        Input::setGet('id', $intId);
        Input::setGet('act', 'toggle');

        if ($dc) {
            $dc->id = $intId; // see #8043
        }

        // Trigger the onload_callback
        if (\is_array($GLOBALS['TL_DCA']['tl_news']['config']['onload_callback'])) {
            foreach ($GLOBALS['TL_DCA']['tl_news']['config']['onload_callback'] as $callback) {
                if (\is_array($callback)) {
                    $this->import($callback[0]);
                    $this->{$callback[0]}->{$callback[1]}($dc);
                } elseif (\is_callable($callback)) {
                    $callback($dc);
                }
            }
        }

        // Check the field access
        if (!$this->User->hasAccess('tl_news::published', 'alexf')) {
            throw new AccessDeniedException('Not enough permissions to publish/unpublish news item ID '.$intId.'.');
        }

        // Set the current record
        if ($dc) {
            $objRow = $this->Database->prepare('SELECT * FROM tl_news WHERE id=?')
                ->limit(1)
                ->execute($intId)
            ;

            if ($objRow->numRows) {
                $dc->activeRecord = $objRow;
            }
        }

        $objVersions = new Versions('tl_news', $intId);
        $objVersions->initialize();

        // Trigger the save_callback
        if (\is_array($GLOBALS['TL_DCA']['tl_news']['fields']['published']['save_callback'])) {
            foreach ($GLOBALS['TL_DCA']['tl_news']['fields']['published']['save_callback'] as $callback) {
                if (\is_array($callback)) {
                    $this->import($callback[0]);
                    $blnVisible = $this->{$callback[0]}->{$callback[1]}($blnVisible, $dc);
                } elseif (\is_callable($callback)) {
                    $blnVisible = $callback($blnVisible, $dc);
                }
            }
        }

        $time = time();

        // Update the database
        $objNews = NewsModel::findByPk($intId);
        $objNews->tstamp = $time;
        $objNews->published = ($blnVisible) ? '1' : '';
        $objNews->save();

        if ($dc) {
            $dc->activeRecord->tstamp = $time;
            $dc->activeRecord->published = ($blnVisible ? '1' : '');
        }

        // Trigger the onsubmit_callback
        if (\is_array($GLOBALS['TL_DCA']['tl_news']['config']['onsubmit_callback'])) {
            foreach ($GLOBALS['TL_DCA']['tl_news']['config']['onsubmit_callback'] as $callback) {
                if (\is_array($callback)) {
                    $this->import($callback[0]);
                    $this->{$callback[0]}->{$callback[1]}($dc);
                } elseif (\is_callable($callback)) {
                    $callback($dc);
                }
            }
        }

        $objVersions->create();

        // The onsubmit_callback has triggered scheduleUpdate(), so run generateFeed() now
        $this->generateFeed();
    }

    /**
     * Check for modified news feeds and update the XML files if necessary.
     */
    public function generateFeed(): void
    {
        /** @var SessionInterface $objSession */
        $objSession = System::getContainer()->get('session');

        $session = $objSession->get('news_feed_updater');

        if (empty($session) || !\is_array($session)) {
            return;
        }

        $this->import('Contao\News', 'News');

        foreach ($session as $id) {
            $this->News->generateFeedsByArchive($id);
        }

        $this->import('Contao\Automator', 'Automator');
        $this->Automator->generateSitemap();

        $objSession->set('news_feed_updater', null);
    }

    /**
     * Return the "preview-button".
     *
     * @param array
     * @param string
     * @param string
     * @param string
     * @param string
     * @param string
     *
     * @return string
     */
    public function checkPreviewIcon($row, $href, $label, $title, $icon, $attributes)
    {
        //test rms
        $objRms = RmsModel::findRef('tl_news', $row['id']);
        if (null === $objRms) {
            return'';
        }

        $previewLink = RmsHelper::getInstance()->getPreviewLink($row['id'], 'tl_news');

        return '<a href="'.$previewLink.'" title="'.StringUtil::specialchars($title).'"'.$attributes.'>'
            .Image::getHtml($icon, $label).'</a> ';
    }

    /**
     * add RMS-Fields in menny content-elements (DCA).
     *
     * @var object
     */
    public function addRmsFields(DataContainer $dc): void
    {
        $strTable = Input::get('table');

        //defined blacklist palettes
        $rm_palettes_blacklist = ['__selector__'];

        //add Field in meny content-elements
        foreach ($GLOBALS['TL_DCA'][$strTable]['palettes'] as $name => $field) {
            if (\in_array($name, $rm_palettes_blacklist, true)) {
                continue;
            }

            $GLOBALS['TL_DCA'][$strTable]['palettes'][$name] .= ';{rms_legend:hide},rms_notice,rms_release_info';
        }
    }

    /**
     * custom modify the rms-Preview
     * used from rmsHelper->modifyForPreview() -> is a parseTemplate->HOOK.
     *
     * @param object
     * @param array
     *
     * @return object
     */
    public function modifyForPreview($objTemplate, $newArr)
    {
        global $objPage;

        $objOriginal = clone $objTemplate;

        if (\is_array($newArr) && \count($newArr) > 0) {
            foreach ($newArr as $k => $v) {
                $objTemplate->$k = $v;
            }

            $objTemplate->newsHeadline = $objTemplate->headline;
            $objTemplate->linkHeadline = str_replace(
                $objOriginal->headline,
                $objTemplate->headline,
                $objTemplate->linkHeadline
            );
            $objTemplate->date = \Date::parse($objPage->datimFormat, $objTemplate->time);

            //author
            $objAuthor = UserModel::findByPk($objTemplate->author);
            if (null !== $objAuthor) {
                $objTemplate->author = $GLOBALS['TL_LANG']['MSC']['by'].' '.$objAuthor->name;
            }
        }

        return $objTemplate;
    }
}
