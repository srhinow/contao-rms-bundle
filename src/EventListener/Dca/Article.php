<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension contao-rms-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

namespace Srhinow\ContaoRmsBundle\EventListener\Dca;

use Contao\Backend;
use Contao\DataContainer;
use Contao\Image;
use Contao\StringUtil;
use Contao\System;
use Srhinow\ContaoRmsBundle\Helper\RmsHelper;
use Srhinow\ContaoRmsBundle\Model\RmsModel;

class Article extends Backend
{
    /**
     * Import the back end user object.
     */
    public function __construct()
    {
        parent::__construct();
        $this->import('BackendUser', 'User');
        $this->RmsHelper = RmsHelper::getInstance();
        $this->logger = System::getContainer()->get('monolog.logger.contao');
    }

    /**
     * Return the "toggle preview-button".
     *
     * @param array
     * @param string
     * @param string
     * @param string
     * @param string
     * @param string
     *
     * @return string
     */
    public function checkPreviewIcon($row, $href, $label, $title, $icon, $attributes)
    {
        $previewLink = $this->RmsHelper->getPreviewLink($row['id'], 'tl_article');

        //test rms
        $objRms = RmsModel::findRef('tl_article', $row['id']);
        if (null === $objRms) {
            return'';
        }

        return '<a href="'.$previewLink.'" title="'.StringUtil::specialchars($title).'"'.$attributes.'>'
            .Image::getHtml($icon, $label).'</a> ';
    }

    public function addRmsChangeLabelHint(array $row, string $label, DataContainer $dc, string $imageAttribute = '', bool $returnImage = false, ?bool $isProtected = null)
    {
        $Article = new \tl_article();
        $label = $Article->addIcon($row, $label);

        if ('1' === $row['rms_new_edit']) {
            $label .= '<span style="color:red;"> ('.$GLOBALS['TL_LANG']['MSC']['rms_new_edit'][0].')</span>';
        }

        return $label;
    }

    /**
     * add RMS-Fields in menny content-elements (DCA).
     *
     * @var object
     */
    public function addRmsFields(DataContainer $dc): void
    {
        //defined blacklist palettes
        $rm_palettes_blacklist = ['__selector__'];

        //add Field in meny content-elements
        foreach ($GLOBALS['TL_DCA']['tl_article']['palettes'] as $name => $field) {
            if (\in_array($name, $rm_palettes_blacklist, true)) {
                continue;
            }

            $GLOBALS['TL_DCA']['tl_article']['palettes'][$name] .= '
            ;{rms_settings_legend:hide},rms_protected
            ;{rms_legend:hide},rms_notice,rms_release_info';
        }
    }
}
