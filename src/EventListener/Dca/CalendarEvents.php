<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension contao-rms-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

namespace Srhinow\ContaoRmsBundle\EventListener\Dca;

use Contao\Backend;
use Contao\CalendarEventsModel;
use Contao\CoreBundle\Exception\AccessDeniedException;
use Contao\DataContainer;
use Contao\Image;
use Contao\Input;
use Contao\StringUtil;
use Contao\System;
use Contao\Versions;
use Srhinow\ContaoRmsBundle\Helper\RmsHelper;
use Srhinow\ContaoRmsBundle\Model\RmsModel;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class CalendarEvents extends Backend
{
    /**
     * @var object|null
     */
    protected $logger;

    public function __construct()
    {
        parent::__construct();
        $this->import('BackendUser', 'User');
        $this->import('Database');
        $this->logger = System::getContainer()->get('monolog.logger.contao');
    }

    /**
     * Return the "toggle-button".
     *
     * @param array
     * @param string
     * @param string
     * @param string
     * @param string
     * @param string
     *
     * @return string
     */
    public function toggleIcon($row, $href, $label, $title, $icon, $attributes)
    {
        //test rms
        $objRms = RmsModel::findRef('tl_calendar_events', $row['id']);
        if (null === $objRms) {
            return'';
        }

        if (Input::get('tid')) {
            $this->toggleVisibility(Input::get('tid'), (1 === Input::get('state')));
            $this->redirect($this->getReferer());
        }

        // Check permissions AFTER checking the tid, so hacking attempts are logged
        if (!$this->User->isAdmin && !$this->User->hasAccess('tl_news::published', 'alexf')) {
            return '';
        }

        $href .= '&amp;tid='.$row['id'].'&amp;state='.($row['published'] ? '' : 1);

        if (!$row['published']) {
            $icon = 'invisible.gif';
        }

        return '<a href="'.$this->addToUrl($href).'" title="'.StringUtil::specialchars($title).'"'.$attributes.'>'
            .Image::getHtml($icon, $label).'</a> ';
    }

    /**
     * Disable/enable a user group.
     *
     * @param int           $intId
     * @param bool          $blnVisible
     * @param DataContainer $dc
     *
     * @throws AccessDeniedException
     */
    public function toggleVisibility($intId, $blnVisible, DataContainer $dc = null): void
    {
        // Set the ID and action
        Input::setGet('id', $intId);
        Input::setGet('act', 'toggle');

        if ($dc) {
            $dc->id = $intId; // see #8043
        }

        // Trigger the onload_callback
        if (\is_array($GLOBALS['TL_DCA']['tl_calendar_events']['config']['onload_callback'])) {
            foreach ($GLOBALS['TL_DCA']['tl_calendar_events']['config']['onload_callback'] as $callback) {
                if (\is_array($callback)) {
                    $this->import($callback[0]);
                    $this->{$callback[0]}->{$callback[1]}($dc);
                } elseif (\is_callable($callback)) {
                    $callback($dc);
                }
            }
        }

        // Check the field access
        if (!$this->User->hasAccess('tl_calendar_events::published', 'alexf')) {
            throw new AccessDeniedException('Not enough permissions to publish/unpublish event ID '.$intId.'.');
        }

        // Set the current record
        if ($dc) {
            $objRow = CalendarEventsModel::findByPk($intId);
            if (null !== $objRow) {
                $dc->activeRecord = $objRow;
            }
        }

        $objVersions = new Versions('tl_calendar_events', $intId);
        $objVersions->initialize();

        // Trigger the save_callback
        if (\is_array($GLOBALS['TL_DCA']['tl_calendar_events']['fields']['published']['save_callback'])) {
            foreach ($GLOBALS['TL_DCA']['tl_calendar_events']['fields']['published']['save_callback'] as $callback) {
                if (\is_array($callback)) {
                    $this->import($callback[0]);
                    $blnVisible = $this->{$callback[0]}->{$callback[1]}($blnVisible, $dc);
                } elseif (\is_callable($callback)) {
                    $blnVisible = $callback($blnVisible, $dc);
                }
            }
        }

        $time = time();

        // Update the database
        $this->Database
            ->prepare("UPDATE tl_calendar_events SET tstamp=$time, 
                              published='".($blnVisible ? '1' : '')."' WHERE id=?")
            ->execute($intId)
        ;

        if ($dc) {
            $dc->activeRecord->tstamp = $time;
            $dc->activeRecord->published = ($blnVisible ? '1' : '');
        }

        // Trigger the onsubmit_callback
        if (\is_array($GLOBALS['TL_DCA']['tl_calendar_events']['config']['onsubmit_callback'])) {
            foreach ($GLOBALS['TL_DCA']['tl_calendar_events']['config']['onsubmit_callback'] as $callback) {
                if (\is_array($callback)) {
                    $this->import($callback[0]);
                    $this->{$callback[0]}->{$callback[1]}($dc);
                } elseif (\is_callable($callback)) {
                    $callback($dc);
                }
            }
        }

        $objVersions->create();

        // The onsubmit_callback has triggered scheduleUpdate(), so run generateFeed() now
        $this->generateFeed();
    }

    /**
     * Check for modified calendar feeds and update the XML files if necessary.
     */
    public function generateFeed(): void
    {
        /** @var SessionInterface $objSession */
        $objSession = System::getContainer()->get('session');

        $session = $objSession->get('calendar_feed_updater');

        if (empty($session) || !\is_array($session)) {
            return;
        }

        $this->import('Contao\Calendar', 'Calendar');

        foreach ($session as $id) {
            $this->Calendar->generateFeedsByCalendar($id);
        }

        $this->import('Contao\Automator', 'Automator');
        $this->Automator->generateSitemap();

        $objSession->set('calendar_feed_updater', null);
    }

    /**
     * Return the "toggle preview-button".
     *
     * @param array
     * @param string
     * @param string
     * @param string
     * @param string
     * @param string
     *
     * @return string
     */
    public function checkPreviewIcon($row, $href, $label, $title, $icon, $attributes)
    {
        $previewLink = RmsHelper::getInstance()->getPreviewLink($row['id'], 'tl_calendar_events');

        //test rms
        $objRms = RmsModel::findRef('tl_calendar_events', $row['id']);
        if (null === $objRms) {
            return'';
        }

        return '<a href="'.$previewLink.'" title="'.StringUtil::specialchars($title).'"'.$attributes.'>'
            .Image::getHtml($icon, $label).'</a> ';
    }

    /**
     * add RMS-Fields in many content-elements (DCA).
     *
     * @var object
     */
    public function addRmsFields(DataContainer $dc): void
    {
        $strTable = Input::get('table');

        //defined blacklist palettes
        $rm_palettes_blacklist = ['__selector__'];

        foreach ($GLOBALS['TL_DCA'][$strTable]['palettes'] as $name => $field) {
            if (\in_array($name, $rm_palettes_blacklist, true)) {
                continue;
            }

            $GLOBALS['TL_DCA'][$strTable]['palettes'][$name] .= ';{rms_legend:hide},rms_notice,rms_release_info';
        }
    }
}
