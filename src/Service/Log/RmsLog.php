<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension contao-rms-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

namespace Srhinow\ContaoRmsBundle\Service\Log;

use Contao\BackendUser;
use Contao\CoreBundle\Framework\ContaoFramework;
use Contao\Model;
use Contao\StringUtil;
use Contao\System;
use Contao\UserModel;
use Srhinow\ContaoRmsBundle\Model\RmsLogModel;
use Srhinow\ContaoRmsBundle\Model\RmsModel;
use Srhinow\ContaoRmsBundle\Versions\RmsVersions;

class RmsLog
{
    protected $framework;

    protected $container;

    public function __construct(ContaoFramework $framework)
    {
        $this->framework = $framework;
        $this->container = System::getContainer();
    }

    /**
     * Erstellt einen neuen Rms-Log Eintrag immer wenn eine Freigabe live gestellt wird.
     *
     * @return bool|int
     */
    public static function newEntry(RmsModel $objRms)
    {
        $strModelClass = Model::getClassFromTable($objRms->ref_table);
        if (!class_exists($strModelClass)) {
            return false;
        }

        /** @var Model $strModelClass */
        if (null === ($objReference = $strModelClass::findByPk($objRms->ref_id))) {
            return false;
        }

        if (null === ($objAutor = UserModel::findByPk($objRms->ref_author))) {
            return false;
        }

        if (null === ($objChangeUser = UserModel::findByPk(BackendUser::getInstance()->id))) {
            return false;
        }

        $objRmsLog = new RmsLogModel();
        $objRmsLog->tstamp = time();
        $objRmsLog->ref_id = $objRms->ref_id;
        $objRmsLog->ref_table = $objRms->ref_table;
        $objRmsLog->author = $objAutor->name.' ('.$objAutor->email.')';
        $objRmsLog->authorUserName = $objAutor->username;
        $objRmsLog->authorId = $objAutor->id;
        $objRmsLog->setLiveUser = $objChangeUser->name.' ('.$objChangeUser->email.')';
        $objRmsLog->setLiveUserName = $objChangeUser->username;
        $objRmsLog->setLiveUserId = $objChangeUser->id;
        $objRmsLog->rms_first_save = $objReference->rms_first_save;
        $objRmsLog->data_old = $objReference->row();
        $objRmsLog->data_new = $objRms->data;
        $objRmsLog->save();

        self::setDiffField($objRmsLog);

        return $objRmsLog->id;
    }

    public static function setDiffField(RmsLogModel $objRmsLog): void
    {
        if (null !== $objRmsLog->diff) {
            return;
        }

        $objVersions = new RmsVersions($objRmsLog->ref_table);
        $arrFrom = StringUtil::deserialize($objRmsLog->data_old);
        $arrTo = StringUtil::deserialize($objRmsLog->data_new);
        $objVersions->compare($arrFrom, $arrTo);

        $objRmsLog->diff = $objVersions->getArrCompare();
        $objRmsLog->save();
    }
}
