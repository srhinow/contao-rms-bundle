<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension contao-rms-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

/*
 * Fields.
 */
$GLOBALS['TL_LANG']['MSC']['rms_new_edit'] = [
    'Changes without approval',
    'This content has been modified but not yet released.',
];

$GLOBALS['TL_LANG']['MSC']['rms_notice'] = [
    'Release Note',
    'The note is the person responsible to assist the changes.',
];

$GLOBALS['TL_LANG']['MSC']['rms_release_info'] = [
    'send information about change',
    'If this box is selected, the release-admin will be notified by e-mail.',
];

$GLOBALS['TL_LANG']['MSC']['rms_release_ok'] = [
    'share this release',
    'If this checkbox is selected, this version is released on the website.',
];

$GLOBALS['TL_LANG']['MSC']['show_preview'] = [
    'show preview',
    'shows the current Web site with all uncommitted changes.',
];

$GLOBALS['TL_LANG']['MSC']['rms_protected'] = [
    'Include this area in the release management',
    'The contents of this field are taken into the module (rms).',
];

$GLOBALS['TL_LANG']['MSC']['rms_master_member'] = [
    'responsible for releases',
    'Select the person who is responsible for the area.',
];

$GLOBALS['TL_LANG']['MSC']['rms_preview_jumpTo'] = [
    'Preview page',
    'If this field is left empty, the "redirect page" is taken as a release preview page. 
    This can be overridden here, however.',
];

$GLOBALS['TL_LANG']['MSC']['rms_email_subject_question'] = 'release request';

$GLOBALS['TL_LANG']['MSC']['rms_email_subject_answer'] = 'release request (reply)';

$GLOBALS['TL_LANG']['MSC']['rms_email_intro'] = [
    'Dear Sir or Madam,',
    'please note that a new item was inserted / a present item was edited.',
];

$GLOBALS['TL_LANG']['MSC']['rms_email_instruction'] = 'Please review the changes and release or refuse them.';

$GLOBALS['TL_LANG']['MSC']['rms_email_closing'] = 'Best Regards';
