<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension contao-rms-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

/*
 * Legends.
 */
$GLOBALS['TL_LANG']['tl_faq']['rms_legend'] = 'Release notification';
