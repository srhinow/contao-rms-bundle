<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension contao-rms-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

/*
 * global Operation.
 */
$GLOBALS['TL_LANG']['tl_rms']['show_preview'] = [
    'Export',
    'Rechnungen und deren Posten in CSV-Dateien exportieren.',
];

/*
 * Fields
 */
$GLOBALS['TL_LANG']['tl_rms']['tstamp'] = ['Release-Date', 'Date of release request.'];
$GLOBALS['TL_LANG']['tl_rms']['ref_table'] = ['modified table', 'referenced table the amendment.'];
$GLOBALS['TL_LANG']['tl_rms']['ref_author'] = ['Author of the change', ''];
$GLOBALS['TL_LANG']['tl_rms']['ref_id'] = ['ID', 'Unique database ID of the changed record'];
$GLOBALS['TL_LANG']['tl_rms']['ref_notice'] = [
    'Release Note',
    'The note is the person responsible to assist the changes.',
];
$GLOBALS['TL_LANG']['tl_rms']['edit_url'] = ['Path to edit view', ''];
$GLOBALS['TL_LANG']['tl_rms']['data'] = ['changed data', 'serialized data of the data record'];
$GLOBALS['TL_LANG']['tl_rms']['status'] = ['status', 'indicates whether this release was already answered.'];
$GLOBALS['TL_LANG']['tl_rms']['region'][0] = 'region';
$GLOBALS['TL_LANG']['tl_rms']['preview_link'][0] = 'Link to preview';
$GLOBALS['TL_LANG']['tl_rms']['last_edit'][0] = 'last edit';
$GLOBALS['TL_LANG']['tl_rms']['status_options'] = [
    '0' => 'unfinished',
    '1' => 'edited',
    '2' => 'in use',
];

/*
 * Buttons
 */
$GLOBALS['TL_LANG']['tl_rms']['settings'] = ['Release Management-Settings', ''];
$GLOBALS['TL_LANG']['tl_rms']['acknowledge'] = ['Release Change', 'Release this change.'];
$GLOBALS['TL_LANG']['tl_rms']['delete'] = ['delete change', 'delete this change.'];
$GLOBALS['TL_LANG']['tl_rms']['edit'] = ['edit content', 'edit this content'];
$GLOBALS['TL_LANG']['tl_rms']['show_diff'] = ['Show difference', 'View the processed difference.'];

/*
 * Legends
 */
$GLOBALS['TL_LANG']['tl_content']['rm_legend'] = 'Release Management';

/*
* Bereiche
*/
$GLOBALS['TL_LANG']['tl_rms']['sessions']['article_tl_article'] = 'Article';
$GLOBALS['TL_LANG']['tl_rms']['sessions']['article_tl_content'] = 'Article :: element';
$GLOBALS['TL_LANG']['tl_rms']['sessions']['newsletter_tl_newsletter'] = 'Newsletter';
$GLOBALS['TL_LANG']['tl_rms']['sessions']['newsletter_tl_content'] = 'Newsletter :: element';
$GLOBALS['TL_LANG']['tl_rms']['sessions']['calendar_tl_content'] = 'Calendar event :: element';
$GLOBALS['TL_LANG']['tl_rms']['sessions']['calendar_tl_calendar_events'] = 'Calendar :: event';
$GLOBALS['TL_LANG']['tl_rms']['sessions']['news_tl_news'] = 'News';
$GLOBALS['TL_LANG']['tl_rms']['sessions']['news_tl_content'] = 'News :: element';

/*
* Other Text
*/
$GLOBALS['TL_LANG']['tl_rms']['diff_new_content']
    = '<h5>The content has been re-created.</h5> <p>Therefore, there is no version for comparison.</p>';

$GLOBALS['TL_LANG']['tl_rms']['info_new_edit'] = '*new created*';
$GLOBALS['TL_LANG']['tl_rms']['list_empty_notice'] = 'empty';
$GLOBALS['TL_LANG']['tl_rms']['list_email_title'] = 'write email';
$GLOBALS['TL_LANG']['tl_rms']['list_preview_title'] = 'Show Changes as previews on the website.';
