<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension contao-rms-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

/*
 * Fields.
 */
$GLOBALS['TL_LANG']['tl_rms_settings']['control_group'] = [
    'Gruppe mit Freigabeberechtigung',
    'Geben Sie die Benutzergruppe an welche eine Freigabeberechtigung haben soll.',
];
$GLOBALS['TL_LANG']['tl_rms_settings']['fallback_master_member'] = [
    'Fallback-RMS-Redakteur',
    'Falls in irgendeinem Bereich zwar rms aktiviert wurde, aber kein zugehöriger rms-Redakteur existiert, 
    wird dieser hier benachrichtigt.',
];
$GLOBALS['TL_LANG']['tl_rms_settings']['email_from'] = [
    'Absender E-Mail-Adresse',
    'OPTIONAL: Nur wenn dieses Feld ausgefüllt wurde, wird diese Email als Versender verwendet. Ansonsten immer die jeweilige Adresse des Redakteurs.',
];
$GLOBALS['TL_LANG']['tl_rms_settings']['extent_emailto'] = [
    'zusätzliche Empfänger-E-Mail-Adressen',
    'Hier können Sie ein oder mehrere Email-Adressen durch ein Komma getrennt eingeben. 
    Diese Email-Adressen werden zusätzlich bei allen Änderungen benachrichtigt. Das Feld kann leer bleiben.',
];
$GLOBALS['TL_LANG']['tl_rms_settings']['ignore_fields'] = [
    'zu ignorierende Felder in der Vergleichsansicht',
    'Tragen Sie hier, durch ein Komma getrennt die Feldnamen ein die vom rms-Modul ignoriert werden sollen. 
    Der In muss der aus der Datenbank sein (z.B. rms_first_save).',
];
$GLOBALS['TL_LANG']['tl_rms_settings']['ignore_content_types'] = [
    'zu ignorierende Inhaltselemente',
    'Tragen Sie hier, durch ein Komma getrennt die Inhaltselemententypen ein die vom rms-Modul ignoriert werden sollen. 
    Der Inhaltselemententyp muss der aus der Datenbank sein (z.B. colsetStart,colsetPart,colsetEnd).',
];
