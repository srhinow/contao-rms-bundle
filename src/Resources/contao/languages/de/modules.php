<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension contao-rms-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

/*
 * Back end modules.
 */
$GLOBALS['TL_LANG']['MOD']['rms'] = [
    'Freigabe-Anfragen',
    'Dieses Modul erlaubt es Ihnen neue bzw. geänderte Inhalte frei zugeben.',
];
