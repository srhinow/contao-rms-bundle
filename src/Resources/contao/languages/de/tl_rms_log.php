<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension contao-rms-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

//$GLOBALS['TL_LANG']['tl_rms_log']['show_preview'] = [
//'Export',
//'Rechnungen und deren Posten in CSV-Dateien exportieren.',
//];

/*
* Fields
*/
$GLOBALS['TL_LANG']['tl_rms_log']['tstamp'] = ['Freigabe-Datum', 'Datum der Freigabe-Anfrage.'];
$GLOBALS['TL_LANG']['tl_rms_log']['ref_table'] = ['geänderte Tabelle', 'referenzierte Tabelle der Änderung.'];
$GLOBALS['TL_LANG']['tl_rms_log']['ref_author'] = ['Autor der Änderung', ''];
$GLOBALS['TL_LANG']['tl_rms_log']['ref_change_user'] = ['freigegeben durch', ''];
$GLOBALS['TL_LANG']['tl_rms_log']['ref_id'] = ['ID', 'Eindeutige Datenbank-ID des geänderten Datensatzes'];
$GLOBALS['TL_LANG']['tl_rms_log']['authorUserName'] = ['Autor', 'Der Benutzername des Autors der Änderung'];
$GLOBALS['TL_LANG']['tl_rms_log']['author'] = ['Autor', 'Der Autors der Änderung'];
$GLOBALS['TL_LANG']['tl_rms_log']['authorId'] = ['Autor-ID'];
$GLOBALS['TL_LANG']['tl_rms_log']['authorUserName'] = ['Benutzername (Autor)'];
$GLOBALS['TL_LANG']['tl_rms_log']['setLiveUser'] = ['Freigeber-ID', 'Die Person welche die Änderung akzeptiert hat'];
$GLOBALS['TL_LANG']['tl_rms_log']['setLiveUserId'] = ['Freigeber-ID'];
$GLOBALS['TL_LANG']['tl_rms_log']['setLiveUserName'] = ['Benutzername (Freigeber)'];

/*
* Buttons
*/
$GLOBALS['TL_LANG']['tl_rms_log']['delete'] = ['Änderung löschen', 'diese Änderung löschen.'];
$GLOBALS['TL_LANG']['tl_rms_log']['show_diff'] = ['Unterschied anzeigen', 'Den bearbeiteten Unterschied anzeigen.'];
