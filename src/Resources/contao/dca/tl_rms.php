<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension contao-rms-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

/*
 * Table tl_rms.
 */
$GLOBALS['TL_DCA']['tl_rms'] = [
    // Config
    'config' => [
        'dataContainer' => 'Table',
        'closed' => true,
        'notEditable' => true,
        'sql' => [
            'keys' => [
                'id' => 'primary',
            ],
        ],
    ],

    // List
    'list' => [
        'sorting' => [
            'mode' => 2,
            'fields' => ['tstamp DESC', 'id DESC'],
            'panelLayout' => 'filter;sort,search,limit',
        ],
        'label' => [
            'fields' => ['tstamp', 'ref_author', 'ref_table', 'ref_notice', 'rms_new_edit'],
            'label_callback' => [
                'srhinow.contao_rms_bundle.listener.dca.rms',
                'listRecipient',
            ],
        ],
        'global_operations' => [
            'settings' => [
                'href' => 'table=tl_rms_settings&act=edit&id=1',
                'class' => 'navigation rms_settings',
                'attributes' => 'onclick="Backend.getScrollOffset();"',
            ],
            'logs' => [
                'href' => 'table=tl_rms_log',
                'class' => 'header_icon rms_logs',
                'attributes' => 'onclick="Backend.getScrollOffset();"',
            ],
            'all' => [
                'label' => &$GLOBALS['TL_LANG']['MSC']['all'],
                'href' => 'act=select',
                'class' => 'header_edit_all',
                'attributes' => 'onclick="Backend.getScrollOffset();" accesskey="e"',
            ],
        ],
        'operations' => [
            'edit' => [
                'label' => &$GLOBALS['TL_LANG']['tl_rms']['edit'],
                'href' => 'act=edit',
                'icon' => 'edit.gif',
                'button_callback' => [
                    'srhinow.contao_rms_bundle.listener.dca.rms',
                    'editElement',
                ],
                'attributes' => 'class="contextmenu"',
            ],
            'show_diff' => [
                'href' => 'key=show_diff&popup=1',
                'attributes'          => 'onclick="if(!confirm(\'' . ($GLOBALS['TL_LANG']['tl_rms']['show_diff'][0] ?? null) . '\'))return false;Backend.getScrollOffset()"',
                'icon' => 'diff.svg',
                'button_callback' => [
                    'srhinow.contao_rms_bundle.listener.dca.rms',
                    'showDiff',
                ],
            ],
            'delete' => [
                'label' => &$GLOBALS['TL_LANG']['tl_rms']['delete'],
                'href' => 'act=delete',
                'icon' => 'delete.gif',
                'attributes'          => 'onclick="if(!confirm(\'' . ($GLOBALS['TL_LANG']['MSC']['deleteConfirm'] ?? null) . '\'))return false;Backend.getScrollOffset()"',
            ],
            'acknowledge' => [
                'label' => &$GLOBALS['TL_LANG']['tl_rms']['acknowledge'],
                'href' => 'key=acknowledge',
                'icon' => 'ok.gif',
            ],
        ],
    ],

    // Fields
    'fields' => [
        'id' => [
            'sql' => 'int(10) unsigned NOT NULL auto_increment',
        ],
        'tstamp' => [
            'label' => &$GLOBALS['TL_LANG']['tl_rms']['tstamp'],
            'filter' => true,
            'sorting' => true,
            'flag' => 6,
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ],
        'ref_table' => [
            'label' => &$GLOBALS['TL_LANG']['tl_rms']['ref_table'],
            'filter' => false,
            'sorting' => true,
            'reference' => &$GLOBALS['TL_LANG']['tl_rms'],
            'sql' => "varchar(255) NOT NULL default ''",
        ],
        'ref_author' => [
            'label' => &$GLOBALS['TL_LANG']['tl_rms']['ref_author'],
            'filter' => true,
            'foreignKey' => 'tl_user.username',
            'sorting' => true,
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ],
        'ref_id' => [
            'label' => &$GLOBALS['TL_LANG']['tl_rms']['ref_id'],
            'search' => false,
            'filter' => false,
            'sorting' => true,
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ],
        'ref_notice' => [
            'label' => &$GLOBALS['TL_LANG']['tl_rms']['ref_notice'],
            'search' => true,
            'sql' => 'longtext NULL',
        ],
        'do' => [
            'sql' => "varchar(55) NOT NULL default ''",
        ],
        'edit_url' => [
            'sql' => 'longtext NULL',
        ],
        'master_id' => [
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ],
        'master_email' => [
            'sql' => "varchar(255) NOT NULL default ''",
        ],
        'preview_jumpTo' => [
            'sql' => 'longtext NULL',
        ],
        'root_ptable' => [
            'sql' => "varchar(255) NOT NULL default ''",
        ],
        'status' => [
            'label' => &$GLOBALS['TL_LANG']['tl_rms']['status'],
            'filter' => true,
            'sorting' => true,
            'reference' => &$GLOBALS['TL_LANG']['tl_rms']['status_options'],
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ],
        'data' => [
            'label' => &$GLOBALS['TL_LANG']['tl_rms']['data'],
            'search' => false,
            'sql' => 'mediumblob NULL',
        ],
    ],
];
