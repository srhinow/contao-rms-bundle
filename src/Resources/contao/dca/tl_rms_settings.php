<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension contao-rms-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

/*
 * Table tl_rms_settings.
 */
$GLOBALS['TL_DCA']['tl_rms_settings'] = [
    // Config
    'config' => [
        'dataContainer' => 'Table',
        'enableVersioning' => false,
        'sql' => [
            'keys' => [
                'id' => 'primary',
            ],
        ],
        'onload_callback' => [
            ['srhinow.contao_rms_bundle.listener.dca.rms_settings', 'createPropertyEntry'],
            //            ['srhinow.contao_rms_bundle.listener.dca.rms_settings', 'checkPermission']
        ],
    ],
    // List
    'list' => [
        'sorting' => [
            'mode' => 1,
            'fields' => ['mediatyp'],
            'flag' => 12,
            'panelLayout' => 'filter;search,limit',
        ],
        'label' => [
            'fields' => ['title', 'author'],
            'format' => '%s (%s)',
        ],
        'global_operations' => [
            'settings' => [
                'label' => &$GLOBALS['TL_LANG']['tl_rms_settings']['settings'],
                'href' => 'table=tl_bbk_properties&act=edit',
                'class' => 'header_edit_all',
                'attributes' => 'onclick="Backend.getScrollOffset();" accesskey="e"',
            ],
            'all' => [
                'label' => &$GLOBALS['TL_LANG']['MSC']['all'],
                'href' => 'act=select',
                'class' => 'header_edit_all',
                'attributes' => 'onclick="Backend.getScrollOffset();" accesskey="e"',
            ],
        ],
        'operations' => [
            'edit' => [
                'label' => &$GLOBALS['TL_LANG']['tl_rms_settings']['edit'],
                'href' => 'table=tl_story_book_cinema&act=edit',
                'icon' => 'edit.gif',
                'attributes' => 'class="contextmenu"',
            ],
            'editheader' => [
                'label' => &$GLOBALS['TL_LANG']['tl_rms_settings']['editheader'],
                'href' => 'act=edit',
                'icon' => 'header.gif',
                'attributes' => 'class="edit-header"',
            ],
            'copy' => [
                'label' => &$GLOBALS['TL_LANG']['tl_rms_settings']['copy'],
                'href' => 'act=copy',
                'icon' => 'copy.gif',
            ],

            'delete' => [
                'label' => &$GLOBALS['TL_LANG']['tl_rms_settings']['delete'],
                'href' => 'act=delete',
                'icon' => 'delete.gif',
                'attributes'          => 'onclick="if(!confirm(\'' . ($GLOBALS['TL_LANG']['MSC']['deleteConfirm'] ?? null) . '\'))return false;Backend.getScrollOffset()"',
            ],
        ],
    ],

    // Palettes
    'palettes' => [
        'default' => 'control_group,fallback_master_member,email_from,extent_emailto,ignore_content_types,ignore_fields',
    ],

    // Fields
    'fields' => [
        'id' => [
            'sql' => 'int(10) unsigned NOT NULL auto_increment',
        ],
        'tstamp' => [
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ],
        'modify' => [
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ],
        'control_group' => [
            'label' => &$GLOBALS['TL_LANG']['tl_rms_settings']['control_group'],
            'exclude' => true,
            'inputType' => 'radio',
            'foreignKey' => 'tl_user_group.name',
            'eval' => ['mandatory' => true, 'multiple' => false, 'submitOnChange' => true],
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ],
        'fallback_master_member' => [
            'label' => &$GLOBALS['TL_LANG']['tl_rms_settings']['fallback_master_member'],
            'exclude' => true,
            'inputType' => 'select',
            'foreignKey' => 'tl_user.name',
            'options_callback' => ['srhinow.contao_rms_bundle.listener.dca.rms_settings', 'getUserFromControlGroup'],
            'eval' => ['mandatory' => true, 'chosen' => true],
            'sql' => "int(10) unsigned NOT NULL default '0'",
            'relation' => ['type' => 'hasOne', 'load' => 'lazy'],
        ],
        'email_from' => [
            'exclude' => true,
            'inputType' => 'text',
            'eval' => ['rgxp'=>'email', 'tl_class' => 'clr long'],
            'sql' => "varchar(155) NOT NULL default ''",
        ],
        'extent_emailto' => [
            'label' => &$GLOBALS['TL_LANG']['tl_rms_settings']['extent_emailto'],
            'exclude' => true,
            'inputType' => 'text',
            'eval' => ['decodeEntities' => true, 'tl_class' => 'clr long'],
            'sql' => "varchar(155) NOT NULL default ''",
        ],
        'ignore_fields' => [
            'label' => &$GLOBALS['TL_LANG']['tl_rms_settings']['ignore_fields'],
            'exclude' => true,
            'inputType' => 'text',
            'default' => 'rms_first_save',
            'eval' => ['decodeEntities' => true, 'tl_class' => 'clr long'],
            'sql' => "varchar(255) NOT NULL default ''",
        ],
        'ignore_content_types' => [
            'label' => &$GLOBALS['TL_LANG']['tl_rms_settings']['ignore_content_types'],
            'exclude' => true,
            'inputType' => 'text',
            'default' => 'colsetStart', 'colsetPart', 'colsetEnd',
            'eval' => ['decodeEntities' => true, 'tl_class' => 'clr long'],
            'sql' => "varchar(255) NOT NULL default ''",
        ],
    ],
];
