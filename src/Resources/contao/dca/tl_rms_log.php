<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension contao-rms-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

/*
 * Table tl_rms.
 */
$GLOBALS['TL_DCA']['tl_rms_log'] = [
    // Config
    'config' => [
        'dataContainer' => 'Table',
        'closed' => true,
        'notEditable' => true,
        'notCopyable' => true,
        'sql' => [
            'keys' => [
                'id' => 'primary',
            ],
        ],
    ],

    // List
    'list' => [
        'sorting' => [
            'mode' => 2,
            'fields' => ['tstamp DESC', 'id DESC'],
            'panelLayout' => 'filter;sort,search,limit',
        ],
        'label' => [
            'fields' => ['tstamp', 'authorUserName', 'setLiveUserName', 'ref_table'],
            'showColumns' => true,
        ],
        'operations' => [
            'show_diff' => [
                'href' => 'key=show_diff&popup=1',
                'attributes'          => 'onclick="if(!confirm(\'' . ($GLOBALS['TL_LANG']['tl_rms']['show_diff'][0] ?? null) . '\'))return false;Backend.getScrollOffset()"',
                'icon' => 'diff.svg',
                'button_callback' => [
                    'srhinow.contao_rms_bundle.listener.dca.rms_log',
                    'showDiff',
                ],
            ],
        ],
    ],

    // Fields
    'fields' => [
        'id' => [
            'sql' => 'int(10) unsigned NOT NULL auto_increment',
        ],
        'tstamp' => [
            'flag' => 8,
            'eval' => ['mandatory' => true, 'rgxp' => 'datetim'],
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ],
        'ref_id' => [
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ],
        'ref_table' => [
            'search' => true,
            'sql' => "varchar(255) NOT NULL default ''",
        ],
        'author' => [
            'filter' => true,
            'sorting' => true,
            'search' => true,
            'sql' => "char(100) NOT NULL default ''",
        ],
        'authorUserName' => [
            'search' => true,
            'sql' => "char(100) NOT NULL default ''",
        ],
        'authorId' => [
            'search' => true,
            'foreignKey' => 'tl_user.username',
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ],
        'setLiveUser' => [
            'search' => true,
            'sql' => "char(100) NOT NULL default ''",
        ],
        'setLiveUserId' => [
            'sorting' => true,
            'foreignKey' => 'tl_user.username',
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ],
        'setLiveUserName' => [
            'search' => true,
            'foreignKey' => 'tl_user.username',
            'sql' => "char(100) NOT NULL default ''",
        ],
        'data_old' => [
            'sql' => 'blob NULL',
        ],
        'data_new' => [
            'sql' => 'blob NULL',
        ],
        'diff' => [
            'sql' => 'text NULL',
        ],
        'rms_first_save' => [
            'sql' => 'char(1) NULL',
        ],
    ],
];
