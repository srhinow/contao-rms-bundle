<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension contao-rms-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

/*
 * System configuration.
 */
$GLOBALS['TL_DCA']['tl_settings']['palettes']['default'] .= ';{rms_legend:hide},rms_active';

$GLOBALS['TL_DCA']['tl_settings']['fields']['rms_active'] = [
    'label' => &$GLOBALS['TL_LANG']['tl_settings']['rms_active'],
    'exclude' => true,
    'inputType' => 'checkbox',
];
