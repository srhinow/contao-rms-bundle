<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension contao-rms-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

/*
 * Table tl_rms.
 */
$GLOBALS['TL_DCA']['tl_rms_tmp'] = [
    // Config
    'config' => [
        'dataContainer' => 'Table',
        'closed' => true,
        'notEditable' => true,
        'sql' => [
            'keys' => [
                'id' => 'primary',
            ],
        ],
    ],

    // List
    'list' => [
        'sorting' => [
            'mode' => 2,
            'fields' => ['tstamp DESC', 'id DESC'],
            'panelLayout' => 'filter;sort,search,limit',
        ],
        'label' => [
            'fields' => ['tstamp', 'ref_author', 'ref_table', 'ref_notice'],
            'label_callback' => ['tl_rms', 'listRecipient'],
        ],
        'global_operations' => [
            'settings' => [
                'label' => &$GLOBALS['TL_LANG']['tl_rms']['settings'],
                'href' => 'table=tl_rms_settings&act=edit&id=1',
                'class' => 'navigation settings',
                'attributes' => 'onclick="Backend.getScrollOffset();"',
            ],
            'all' => [
                'label' => &$GLOBALS['TL_LANG']['MSC']['all'],
                'href' => 'act=select',
                'class' => 'header_edit_all',
                'attributes' => 'onclick="Backend.getScrollOffset();" accesskey="e"',
            ],
        ],
        'operations' => [
            'edit' => [
                'label' => &$GLOBALS['TL_LANG']['tl_article']['edit'],
                'href' => 'act=edit',
                'icon' => 'edit.gif',
                'button_callback' => ['tl_rms', 'editArticle'],
                'attributes' => 'class="contextmenu"',
            ],
            'delete' => [
                'label' => &$GLOBALS['TL_LANG']['tl_rms']['delete'],
                'href' => 'act=delete',
                'icon' => 'delete.gif',
                'attributes'          => 'onclick="if(!confirm(\'' . ($GLOBALS['TL_LANG']['MSC']['deleteConfirm'] ?? null) . '\'))return false;Backend.getScrollOffset()"',
            ],
            'acknowledge' => [
                'label' => &$GLOBALS['TL_LANG']['tl_rms']['acknowledge'],
                'href' => 'key=acknowledge',
                'icon' => 'ok.gif',
            ],
        ],
    ],

    // Fields
    'fields' => [
        'id' => [
            'sql' => 'int(10) unsigned NOT NULL auto_increment',
        ],
        'pid' => [
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ],
        'tstamp' => [
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ],
        'ref_table' => [
            'sql' => "varchar(255) NOT NULL default ''",
        ],
        'ref_author' => [
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ],
        'ref_id' => [
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ],
        'status' => [
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ],
        'data' => [
            'sql' => 'mediumblob NULL',
        ],
    ],
];
