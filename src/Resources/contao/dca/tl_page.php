<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension contao-rms-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

/*
 * Table tl_page.
 */
if (\Contao\Config::get('rms_active')) {
    \Contao\Controller::loadLanguageFile('tl_default');

    // Palettes
    $GLOBALS['TL_DCA']['tl_page']['palettes']['__selector__'][] = 'rms_protected';
    $GLOBALS['TL_DCA']['tl_page']['palettes']['root'] .= ';{rms_legend:hide},rms_protected';
    $GLOBALS['TL_DCA']['tl_page']['palettes']['rootfallback'] .= ';{rms_legend:hide},rms_protected';

    // Subpalettes
    $GLOBALS['TL_DCA']['tl_page']['subpalettes']['rms_protected'] = 'rms_master_member';
}

// Fields
$GLOBALS['TL_DCA']['tl_page']['fields']['rms_protected'] = [
    'label' => &$GLOBALS['TL_LANG']['MSC']['rms_protected'],
    'exclude' => true,
    'filter' => true,
    'inputType' => 'checkbox',
    'eval' => ['submitOnChange' => true],
    'sql' => "char(1) NOT NULL default ''",
];
$GLOBALS['TL_DCA']['tl_page']['fields']['rms_master_member'] = [
    'label' => &$GLOBALS['TL_LANG']['MSC']['rms_master_member'],
    'exclude' => true,
    'inputType' => 'select',
    'foreignKey' => 'tl_user.name',
    'eval' => ['mandatory' => true, 'chosen' => true, 'tl_class' => 'w50'],
    'sql' => "int(10) unsigned NOT NULL default '0'",
    'relation' => ['type' => 'hasOne', 'load' => 'lazy'],
];
