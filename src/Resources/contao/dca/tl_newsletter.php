<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension contao-rms-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

/*
 * Table tl_newsletter.
 */
if (\Contao\Config::get('rms_active')) {
    \Contao\Controller::loadLanguageFile('tl_default');

    /*
     * change dca from tl_newsletter
     */
    $GLOBALS['TL_DCA']['tl_newsletter']['config']['onload_callback'][] = [
        'srhinow.contao_rms_bundle.listener.dca.newsletter',
        'addRmsFields',
    ];
    $GLOBALS['TL_DCA']['tl_newsletter']['list']['operations']['send']['button_callback'] = [
        'srhinow.contao_rms_bundle.listener.dca.newsletter',
        'checkSendIcon',
    ];

    /*
    * add operation show Preview
    */
    $GLOBALS['TL_DCA']['tl_newsletter']['list']['operations']['showPreview'] = [
        'label' => &$GLOBALS['TL_LANG']['tl_calendar_events']['show_preview'],
        'href' => 'key=showPreview',
        'class' => 'browser_preview',
        'icon' => 'page.gif',
        'attributes' => 'target="_blank"',
        'button_callback' => [
            'srhinow.contao_rms_bundle.listener.dca.newsletter',
            'checkPreviewIcon',
        ],
    ];
}

/*
* Fields
*/
$GLOBALS['TL_DCA']['tl_newsletter']['fields']['ptable']['ignoreDiff'] = true;

$GLOBALS['TL_DCA']['tl_newsletter']['fields']['rms_first_save'] = [
    'sql' => "char(1) NOT NULL default ''",
];

$GLOBALS['TL_DCA']['tl_newsletter']['fields']['rms_new_edit'] = [
    'sql' => "char(1) NOT NULL default ''",
    'ignoreDiff' => true,
];

$GLOBALS['TL_DCA']['tl_newsletter']['fields']['rms_ref_table'] = [
    'sql' => "char(55) NOT NULL default ''",
    'ignoreDiff' => true,
];

$GLOBALS['TL_DCA']['tl_newsletter']['fields']['rms_notice'] = [
    'label' => &$GLOBALS['TL_LANG']['MSC']['rms_notice'],
    'exclude' => true,
    'search' => true,
    'inputType' => 'textarea',
    'eval' => ['mandatory' => false, 'rte' => false],
    'sql' => 'longtext NULL',
];

$GLOBALS['TL_DCA']['tl_newsletter']['fields']['rms_release_info'] = [
    'label' => &$GLOBALS['TL_LANG']['MSC']['rms_release_info'],
    'exclude' => true,
    'inputType' => 'checkbox',
    'sql' => "char(1) NOT NULL default ''",
    'ignoreDiff' => true,
    'save_callback' => [
        ['srhinow.contao_rms_bundle.helper.rms_helper', 'sendEmailInfo'],
    ],
];
