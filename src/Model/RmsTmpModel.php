<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension contao-rms-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

namespace Srhinow\ContaoRmsBundle\Model;

use Contao\Model;

class RmsTmpModel extends Model
{
    /**
     * Table name.
     *
     * @var string
     */
    protected static $strTable = 'tl_rms_tmp';

    /**
     * findet den orignalen gesicherten und bereits veroeffentlichen Datensatz nach UserId refID und table.
     *
     * @param string $strRefTable
     * @param int    $strRefId
     * @param int    $userId
     *
     * @return Model|Model\Collection|null |null
     */
    public static function findRefByUser($strRefTable = '', $strRefId = 0, $userId = 0)
    {
        if (\strlen($strRefTable) < 1 || $strRefId < 1) {
            return null;
        }

        $t = static::$strTable;

        $arrColumns[] = "$t.`ref_table`=?";
        $arrValues[] = $strRefTable;
        $arrColumns[] = "$t.`ref_id`=?";
        $arrValues[] = $strRefId;
        $arrColumns[] = "$t.`ref_author`=?";
        $arrValues[] = $userId;

        return parent::findOneBy($arrColumns, $arrValues);
    }
}
