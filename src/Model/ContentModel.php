<?php
/**
 * Created by rms-contao.
 * Developer: Sven Rhinow (sven@sr-tag.de)
 * Date: 26.02.22
 */

namespace Srhinow\ContaoRmsBundle\Model;

use Contao\Date;
use Contao\Model\Collection;

class ContentModel extends \Contao\ContentModel
{
    /**
     * Find all published content elements by their parent ID and parent table
     *
     * @param integer $intPid         The article ID
     * @param string  $strParentTable The parent table name
     * @param array   $arrOptions     An optional options array
     *
     * @return Collection|\Contao\ContentModel[]|ContentModel|null A collection of models or null if there are no content elements
     */
    public static function findPublishedByPidAndTable($intPid, $strParentTable, array $arrOptions=array())
    {
        $t = static::$strTable;

        // Also handle empty ptable fields
        if ($strParentTable == 'tl_article')
        {
            $arrColumns = array("$t.pid=? AND ($t.ptable=? OR $t.ptable='')");
        }
        else
        {
            $arrColumns = array("$t.pid=? AND $t.ptable=?");
        }

        // Skip unsaved elements (see #2708)
        $arrColumns[] = "$t.tstamp!=0";

        if (!isset($arrOptions['order']))
        {
            $arrOptions['order'] = "$t.sorting";
        }

        return static::findBy($arrColumns, array($intPid, $strParentTable), $arrOptions);
    }
}