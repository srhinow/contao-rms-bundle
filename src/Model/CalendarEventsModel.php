<?php
/**
 * Created by rms-contao.
 * Developer: Sven Rhinow (sven@sr-tag.de)
 * Date: 25.02.22
 */

namespace Srhinow\ContaoRmsBundle\Model;

use Contao\Date;
use Contao\Input;

class CalendarEventsModel extends \Contao\CalendarEventsModel
{
    /**
     * Find a published event from one or more calendars by its ID or alias
     *
     * @param mixed $varId      The numeric ID or alias name
     * @param array $arrPids    An array of calendar IDs
     * @param array $arrOptions An optional options array
     *
     * @return \Contao\CalendarEventsModel|null The model or null if there is no event
     */
    public static function findPublishedByParentAndIdOrAlias($varId, $arrPids, array $arrOptions=array())
    {
        if (empty($arrPids) || !\is_array($arrPids))
        {
            return null;
        }

        $t = static::$strTable;
        $arrColumns = !preg_match('/^[1-9]\d*$/', $varId) ? array("BINARY $t.alias=?") : array("$t.id=?");
        $arrColumns[] = "$t.pid IN(" . implode(',', array_map('\intval', $arrPids)) . ")";

        if (Input::get('do') !== 'preview')
        {
            $time = Date::floorToMinute();
            $arrColumns[] = "$t.published='1' AND ($t.start='' OR $t.start<='$time') AND ($t.stop='' OR $t.stop>'$time')";
        }

        return static::findOneBy($arrColumns, $varId, $arrOptions);
    }
}