<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension contao-rms-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

namespace Srhinow\ContaoRmsBundle\Model;

use Contao\Model;

/**
 * @property int    $id
 * @property int    $tstamp
 * @property string $ref_id
 * @property string $ref_table
 * @property string $ref_author
 * @property int    $ref_notice
 * @property bool   $do
 * @property string $edit_url
 * @property string $master_id
 * @property int    $master_email
 * @property bool   $preview_jumpTo
 * @property bool   $root_ptable
 * @property bool   $status
 * @property bool   $data
 */
class RmsModel extends Model
{
    /**
     * Table name.
     *
     * @var string
     */
    protected static $strTable = 'tl_rms';

    /**
     * findet ein noch nicht freigegebenen Inhalt nach ref-ID und table.
     *
     * @param string $strRefTable
     * @param int    $strRefId
     *
     * @return Model|Model\Collection|null
     */
    public static function findRef($strRefTable = '', $strRefId = 0)
    {
        if (\strlen($strRefTable) < 1 || $strRefId < 1) {
            return null;
        }

        $t = static::$strTable;

        $arrColumns[] = "$t.`ref_table`=?";
        $arrValues[] = $strRefTable;
        $arrColumns[] = "$t.`ref_id`=?";
        $arrValues[] = $strRefId;

        return parent::findOneBy($arrColumns, $arrValues);
    }

    /**
     * findet ein noch nicht freigegebenen Inhalt nach UserId refID und table.
     *
     * @param string $strRefTable
     * @param int    $strRefId
     * @param int    $userId
     *
     * @return Model|Model\Collection|null
     */
    public static function findRefByUser($strRefTable = '', $strRefId = 0, $userId = 0)
    {
        if (\strlen($strRefTable) < 1 || $strRefId < 1) {
            return null;
        }

        $t = static::$strTable;

        $arrColumns[] = "$t.`ref_table`=?";
        $arrValues[] = $strRefTable;
        $arrColumns[] = "$t.`ref_id`=?";
        $arrValues[] = $strRefId;
        $arrColumns[] = "$t.`ref_author`=?";
        $arrValues[] = $userId;

        return parent::findOneBy($arrColumns, $arrValues);
    }
}
