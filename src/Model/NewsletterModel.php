<?php
/**
 * Created by rms-contao.
 * Developer: Sven Rhinow (sven@sr-tag.de)
 * Date: 25.02.22
 */

namespace Srhinow\ContaoRmsBundle\Model;

use Contao\Input;

class NewsletterModel extends \Contao\NewsletterModel
{
    /**
     * Find a sent newsletter by its parent IDs and its ID or alias
     *
     * @param integer $varId      The numeric ID or alias name
     * @param array   $arrPids    An array of newsletter channel IDs
     * @param array   $arrOptions An optional options array
     *
     * @return \Contao\NewsletterModel|null The model or null if there are no sent newsletters
     */
    public static function findSentByParentAndIdOrAlias($varId, $arrPids, array $arrOptions=array())
    {
        if (empty($arrPids) || !\is_array($arrPids))
        {
            return null;
        }

        $t = static::$strTable;
        $arrColumns = !preg_match('/^[1-9]\d*$/', $varId) ? array("BINARY $t.alias=?") : array("$t.id=?");
        $arrColumns[] = "$t.pid IN(" . implode(',', array_map('\intval', $arrPids)) . ")";

        if (Input::get('do') !== 'preview')
        {
            $arrColumns[] = "$t.sent=1";
        }

        return static::findOneBy($arrColumns, $varId, $arrOptions);
    }
}