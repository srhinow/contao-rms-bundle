<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension contao-rms-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

namespace Srhinow\ContaoRmsBundle\Model;

use Contao\Model;

/**
 * @property int    $id
 * @property int    $tstamp
 * @property int    $ref_id
 * @property string $ref_table
 * @property string $author
 * @property string $authorUserName
 * @property int    $authorId
 * @property string $setLiveUser
 * @property int    $setLiveUserName
 * @property int    $setLiveUserId
 * @property string $rms_first_save
 * @property string $data_old
 * @property string $data_new
 * @property string $diff
 */
class RmsLogModel extends Model
{
    /**
     * Table name.
     *
     * @var string
     */
    protected static $strTable = 'tl_rms_log';
}
