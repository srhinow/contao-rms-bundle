<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension contao-rms-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

namespace Srhinow\ContaoRmsBundle\Helper;

use Contao\ArticleModel;
use Contao\Backend;
use Contao\BackendUser;
use Contao\CalendarEventsModel;
use Contao\CalendarModel;
use Contao\Config;
use Contao\ContentModel;
use Contao\Controller;
use Contao\Database;
use Contao\Database\Result;
use Contao\DataContainer;
use Contao\Email;
use Contao\FaqCategoryModel;
use Contao\FaqModel;
use Contao\Input;
use Contao\Model;
use Contao\NewsArchiveModel;
use Contao\NewsletterChannelModel;
use Contao\NewsletterModel;
use Contao\NewsModel;
use Contao\PageModel;
use Contao\StringUtil;
use Contao\UserModel;
use Srhinow\ContaoRmsBundle\Model\RmsModel;
use Srhinow\ContaoRmsBundle\Model\RmsSettingsModel;

class RmsHelper extends Backend
{
    // instance
    protected static $instance = null;

    /**
     * hold rms settings as array.
     *
     * @var array
     */
    protected $settings = [];

    protected $logger;

    /**
     * Constructor.
     */
    public function __construct()
    {
        parent::__construct();

        // Import
        $this->import('BackendUser', 'User');
        $this->logger = Controller::getContainer()->get('monolog.logger.contao');
        $this->settings = self::defineSettings();

        // if not set -> set default values
        if (!$this->settings['control_group']) {
            $this->settings['control_group'] = 0;
        }

        // deaktiviere Freigabefunktion, wenn der Schalter nicht existiert
        if (!$GLOBALS['TL_CONFIG']['rms_active']) {
            Config::getInstance()->update('rms_active', false);
            $GLOBALS['TL_CONFIG']['rms_active'] = false;
        }
    }

    /**
     * Returns the rmsHelper.
     *
     * @return rmsHelper
     */
    public static function getInstance()
    {
        if (null === self::$instance) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public static function defineSettings()
    {
        // verhindert Fehlermeldungen beim re-installieren wenn 'rms_active' noch aktiv gesetzt ist (#1)
        $Database = Database::getInstance();
        if (!$Database->tableExists('tl_rms_settings')) {
            $GLOBALS['TL_CONFIG']['rms_active'] = false;
            Config::getInstance()->update('rms_active', false);

            return [];
        }

        $objSettings = RmsSettingsModel::findByPk($GLOBALS['RMS']['SETTINGS_ID']);

        return (null === $objSettings) ? [] : $objSettings->row();
    }

    /**
     * ueberprueft ob der Benutzer ohne Freigaberechte ist.
     */
    public function isMemberOfSlaves()
    {
        return (!$this->User->isMemberOf($this->settings['control_group'])
            && !$this->User->isAdmin) ? true : false;
    }

    /**
     * ueberprueft ob der Benutzer mit Freigaberechte ist.
     */
    public function isMemberOfMasters()
    {
        return ($this->User->isMemberOf($this->settings['control_group'])
            || $this->User->isAdmin) ? true : false;
    }

    /**
     * Overwrite db-object with data-array.
     *
     * @var object
     * @var string
     *
     * @return object
     */
    public function overwriteDbObj($origObj, $newArr)
    {
        if (\is_array($newArr) && \count($newArr) > 0) {
            foreach ($newArr as $k => $v) {
                $origObj->{$k} = $v;
            }

            if((int) $origObj->tstamp === 0) {
                $origObj->tstamp =  time();
            }
        }

        return  $origObj;
    }

    /**
     * send Email for new release if Checkbox selected.
     *
     * @var object
     */
    public function sendEmailInfo($varValue, DataContainer $dc)
    {
        if ('1' !== $varValue) {
            return '';
        }

        $this->loadLanguageFile('tl_default');
        $this->import('BackendUser');

        $strTable = Input::get('table') ?: 'tl_'.Input::get('do');

        $RmsSectionSettings = $this->getRmsSectionSettings($dc->id, $strTable, $dc->parentTable);

        if (!$RmsSectionSettings['rms_protected']) {
            return '';
        }

        $fallbackEmail = $this->getMemberData($this->settings['fallback_master_member'], 'email');
        $sendToEmail = ($RmsSectionSettings['master_email']) ? $RmsSectionSettings['master_email'] : $fallbackEmail;

        //mail from editor to Super-Editor (question)
        if (!$this->isMemberOfMasters()) {

            $emailFrom = (strlen(trim($this->settings['email_from'])) > 0)
                ? trim($this->settings['email_from'])
                : $this->BackendUser->email;

            $sendToEmailsArr = (\strlen(trim($this->settings['extent_emailto'])) > 0)
                ? array_map('trim', explode(',', $this->settings['extent_emailto']))
                : [];

            $sendToEmailsArr[] = $sendToEmail;
            $sendToEmailsArr = array_unique($sendToEmailsArr);
            $sendToEmails = implode(',', $sendToEmailsArr);

            $objTemplate = new \FrontendTemplate('mail_review_question');
            $objTemplate->text = $dc->Input->post('rms_notice');
            $objTemplate->link = $this->Environment->url.'/'.$this->addToUrl('&author='.$this->BackendUser->id);


            $email = new Email();
            $email->from = $emailFrom;
            $email->charset = 'utf-8';
            $email->subject = $GLOBALS['TL_LANG']['MSC']['rms_email_subject_question'];
            $email->text = $objTemplate->parse();
            $email->sendTo($sendToEmails);

            $_SESSION['send_rms_info'] = 1;
        } else {
            //send Email from Super-Editor to editor  (answer)
            $text = Input::post('rms_notice');
            $text .= "\nPfad: ".$this->Environment->url.$this->Environment->requestUri;

            $email = new Email();
            $email->from = $this->BackendUser->email;
            $email->charset = 'utf-8';
            $email->subject = $GLOBALS['TL_LANG']['MSC']['rms_email_subject_answer'];
            $email->text = $text;

            if ($authorMail = $this->getMemberData(Input::get('author'), 'email')) {
                $email->sendTo($this->getMemberData(Input::get('author'), 'email'));
            }
        }

        //disable everytime sendEmail
        $this->Database
            ->prepare('UPDATE `'.$strTable.'` SET `rms_release_info`="" WHERE `id`=?')
            ->execute($dc->id)
        ;

        return '';
    }

    /**
     * open Frontend with nooutorised Content.
     */
    public function showPreviewInBrowser(): void
    {
        $this->redirect($this->getPreviewLink('', ''));
    }

    /**
     * get Preview Link-Date.
     *
     * @return string
     */
    public function getPreviewLink($id, string $table)
    {
        if (0 === (int) $id) {
            $id = Input::get('id');
        }

        if ('' === $table) {
            $table = Input::get('table');
        }

        if (!$this->settings) {
            $this->settings = $this->getSettings();
        }

        $objRms = RmsModel::findRef($table, $id);
        if (null === $objRms) {
            return '';
        }

        return $objRms->preview_jumpTo;
    }

    /**
     * get root-Parent-Table for the rms-settings and jumpto for the preview-Link.
     *
     * @param string
     *
     * @return string
     */
    public function getRootParentTable($table, $ptable = '')
    {
        if ('tl_article' === $ptable) {
            return 'tl_page';
        }

        $this->loadDataContainer($table);
        $pTable = $table;

        if (null !== $GLOBALS['TL_DCA'][$pTable]['config']['ptable']) {
            $pTable = $this->getRootParentTable($GLOBALS['TL_DCA'][$pTable]['config']['ptable'], '');
        }

        return $pTable;
    }

    /**
     * get root-Parent-DB-Object for the rms-settings.
     *
     * @param string
     *
     * @return Result The Result object
     */
    public function getRootParentDbObj($id, $table, $ptable, $rtable)
    {
        $mode = Input::get('mode');

        if (!empty($mode) && 'tl_article' === $table) {
            $ptable = 'tl_page';
        }

        if (null === $ptable) {
            $objDb = $this->Database->prepare('SELECT * FROM '.$table.' WHERE `id`=?')
                        ->limit(1)
                        ->execute($id)
            ;
        } else {
            $this->loadDataContainer($ptable);

            $objDb = $this->Database
                ->prepare('SELECT pt.* FROM '.$ptable.' pt  
                LEFT JOIN '.$table.' t ON pt.`id` = t.`pid` 
                WHERE t.`id`=?')
                ->limit(1)
                ->execute($id)
            ;

            if (null !== $GLOBALS['TL_DCA'][$ptable]['config']['ptable']) {
                $objDb = $this->getRootParentDbObj(
                    $objDb->id,
                    $ptable,
                    $GLOBALS['TL_DCA'][$ptable]['config']['ptable'],
                    $rtable
                );
            }
        }

        return $objDb;
    }

    /**
     * get rms_section_settings.
     *
     * @param int
     * @param string
     * @param string
     *
     * @return array
     */
    public function getRmsSectionSettings($id, $table, $ptable)
    {
        // um zu testen ob es tl_page ist oder eine andere, da tl_content auch von Modulen wie News verwendet wird
        $root_table = $this->getRootParentTable($table, $ptable);

        // die Einstellungen zu dem Bereich holen
        $objDb = $this->getRootParentDbObj($id, $table, $ptable, $root_table);

        // den dca der Eltern-Tabelle holen
        if (null !== $ptable) {
            $this->loadDataContainer($ptable);
        }

        // Bereich-Einstellungen als Array erstellen

        // wenn es ein normaler Inhalt einer Seite ist
        if ('tl_page' === $root_table) {
            $rootPage = $this->getRootPage($objDb->id);
            $objPage = PageModel::findByPk($objDb->id);
            $rmsSectionSettings = [
                'rms_protected' => $rootPage->rms_protected,
                'master_id' => $rootPage->rms_master_member,
                'master_email' => ((int) $rootPage->rms_master_member > 0)
                    ? $this->getMemberData($rootPage->rms_master_member, 'email')
                    : $this->getMemberData($this->settings['fallback_master_member']),
                'preview_jumpTo' => $objPage->getFrontendUrl().'?do=preview',
            ];
        } else {
            // wenn es ein Modul wie z.B. News ist
            $rmsSectionSettings = [
                'rms_protected' => $objDb->rms_protected,
                'master_id' => $objDb->rms_master_member,
                'master_email' => ((int) $objDb->rms_master_member > 0)
                    ? $this->getMemberData($objDb->rms_master_member, 'email')
                    : $this->getMemberData($this->settings['fallback_master_member']),
                'preview_jumpTo' => '',
            ];
            // Weiterleitungsseite (ID) ermitteln
            $jumpToID = ((int) $objDb->rms_preview_jumpTo > 0) ? $objDb->rms_preview_jumpTo : $objDb->jumpTo;

            if ($jumpToID > 0) {
                $objPage = PageModel::findByPk($jumpToID);

                if (null !== $ptable) {
                    // wenn es die Content-Ebene ist, dann den Alias der Eltern-Tabelle für den Vorschaulink holen
                    if ('tl_content' === $table) {

                        $moduleObj = $this->Database
                            ->prepare(
                                'SELECT pt.* FROM '.$ptable.' `pt` 
                                LEFT JOIN `tl_content` `c` ON `pt`.`id` = `c`.`pid` 
                                WHERE `c`.`id`=?'
                            )
                            ->limit(1)
                            ->execute($id)
                        ;
                        $alias = $this->fixAliasIfNotSet((int) $moduleObj->id, $ptable);

                    } else {

                        // wenn es die Modulebene ist, den Alias direkt aus der aktuellen Tabelle
                        // für den Vorschaulink holen
                        $moduleObj = $this->Database
                            ->prepare('SELECT * FROM '.$table.'  WHERE `id`=?')
                            ->limit(1)
                            ->execute($id)
                        ;

                        $alias = $this->fixAliasIfNotSet((int) $moduleObj->id, $table);
                    }

                    $strParams = Config::get('useAutoItem') ? '/%s' : '/items/%s';

                    // komplette Vorschau-Url erstellen
                    $rmsSectionSettings['preview_jumpTo'] = $objPage->getFrontendUrl(
                        sprintf($strParams, $alias)
                    ).'?do=preview';
                } else {
                    // komplette Vorschau-Url erstellen
                    $rmsSectionSettings['preview_jumpTo'] = $objPage->getFrontendUrl().'?do=preview';
                }
            }
        }

        // HOOK: add custom logic
        if (isset($GLOBALS['TL_HOOKS']['getRmsSectionSettings'])
            && \is_array($GLOBALS['TL_HOOKS']['getRmsSectionSettings'])) {
            foreach ($GLOBALS['TL_HOOKS']['getRmsSectionSettings'] as $callback) {
                if (\is_array($callback)) {
                    $this->import($callback[0]);
                    $rmsSectionSettings = $this->$callback[0]->$callback[1]($rmsSectionSettings, $id, $table, $ptable);
                } elseif (\is_callable($callback)) {
                    $rmsSectionSettings = $callback($rmsSectionSettings, $id, $table, $ptable);
                }
            }
        }

        return $rmsSectionSettings;
    }

    /**
     * setzt ein Alias falls noch nicht vorhanden und gibt den Alias zurück
     * @param int $id
     * @param string $table
     * @return string
     */
    public function fixAliasIfNotSet(int $id, string $table): string
    {
        if($table === '') {
            return '';
        }

        try{
            $strModelClass = Model::getClassFromTable($table);
            if (!class_exists($strModelClass)) {
                return '';
            }

            /** @var Model $strModelClass */
            if(null === ($objTable = $strModelClass::findByPk($id))) {
                return '';
            }

            // wenn alias bereits existiert diesen dann zurückgeben
            if(strlen($objTable->alias) > 0) {
                return $objTable->alias;
            }

            //ansonsten Alias erstellen, in Tabelle speichern und zurückgeben
            $alias = '';
            switch(Input::get('do')) {
                case 'news':
                    $strTitle = (Input::post('headline'))?:$objTable->headline;
                    $alias = StringUtil::standardize($strTitle);
                    break;
                case 'newsletter':
                    $strTitle = (Input::post('subject'))?:$objTable->subject;
                    $alias = StringUtil::standardize($strTitle);
                case 'calendar_events':
                    $strTitle = (Input::post('title'))?:$objTable->title;
                    $alias = StringUtil::standardize($strTitle);
            }
            $objTable->alias = $alias;
            $objTable->save();

            return $alias;
        } catch(\Exception $e) {
            return '';
        }

    }

    /**
     * get any field from given user-id.
     *
     * @param int
     * @param string
     */
    public function getMemberData($id, $field = '')
    {
        if ((int) $id > 0) {
            $objUser = UserModel::findByPk($id);

            if (\strlen((string) $objUser->{$field}) > 0) {
                return $objUser->{$field};
            }
        }

        return '';
    }

    /**
     * Gibt ein array mit bearbeiteten aber noch nicht freigebenen Daten aus dem Feld dma_data zurueck.
     *
     * @param string $strTable
     * @param object $dc
     *
     * @return array $fields
     */
    public function rewriteDcaFields($strTable, $dc)
    {
        $fields = [];
        $userId = (Input::get('author')) ?: BackendUser::getInstance()->id;

        $objRmsResult = RmsModel::findRefByUser($strTable, $dc->id, $userId);
        if (null === $objRmsResult) {
            return $fields;
        }

        $arrRmsResult = StringUtil::deserialize($objRmsResult->data);

        return $arrRmsResult['dma_eg_data'];
    }

    /**
     * testet auf diverse Module welche die tl_content-Tabelle verwenden.
     *
     * @param string
     *
     * @return bool
     */
    public function isContentRmsProtected($strTable)
    {
        $id = Input::get('id');
        if (null === $id) {
            return false;
        }

        $objContent = ContentModel::findByPk($id);
        if (null === $objContent) {
            return false;
        }

        switch (Input::get('do')) {
            case 'article':
                $objArticle = ArticleModel::findByPk($objContent->pid);
                if (null === $objArticle) {
                    return false;
                }

                $objPage = $objArticle->getRelated('pid');

                $objRootPage = $this->getRootPage($objPage->id);

                // old and new filter
                if ('1' === $objRootPage->rms_protected) {
                    if ($objRootPage->rms_master_member > 0) {
                        $this->settings['sender_email'] = $this->getMemberData(
                            $objRootPage->rms_master_member,
                            'email'
                        );
                    }

                    return true;
                }
                break;
            case 'news':
                $objNews = NewsModel::findByPk($objContent->pid);
                if (null === $objNews) {
                    return false;
                }

                $objNewsArchive = $objNews->getRelated('pid');

                if ('1' === $objNewsArchive->rms_protected) {
                    if ($objNewsArchive->rms_master_member > 0) {
                        $this->settings['sender_email'] = $this->getMemberData(
                            $objNewsArchive->rms_master_member,
                            'email'
                        );
                    }

                    return true;
                }
                break;
            case 'calendar':
                $objCalendarEvents = CalendarEventsModel::findByPk($objContent->pid);
                if (null === $objCalendarEvents) {
                    return false;
                }

                $objCalendar = $objCalendarEvents->getRelated('pid');
                if (null === $objCalendar) {
                    return false;
                }

                if ('1' === $objCalendar->rms_protected) {
                    if ($objCalendar->rms_master_member > 0) {
                        $this->settings['sender_email'] = $this->getMemberData(
                            $objCalendar->rms_master_member,
                            'email'
                        );
                    }

                    return true;
                }
                break;
            default:
                $isProtected = false;
                // HOOK: add custom logic
                if (isset($GLOBALS['TL_HOOKS']['isContentRmsProtected'])
                    && \is_array($GLOBALS['TL_HOOKS']['isContentRmsProtected'])) {
                    foreach ($GLOBALS['TL_HOOKS']['isContentRmsProtected'] as $callback) {
                        if (\is_array($callback)) {
                            $this->import($callback[0]);
                            $isProtected = $this->$callback[0]->{$callback[1]}($strTable);
                        } elseif (\is_callable($callback)) {
                            $isProtected = $callback($strTable);
                        }

                        if (\is_bool($isProtected)) {
                            return $isProtected;
                        }
                    }
                }
        }

        return false;
    }

    /**
     * testet auf diverse Tabellen ob diese als geschützt markiert wurden.
     *
     * @param string
     *
     * @return bool
     */
    public function isTableRmsProtected($strTable)
    {
        $id = Input::get('id');
        if (null === $id) {
            return false;
        }

        switch ($strTable) {
            case 'tl_article':
                if (\in_array('tl_article', $GLOBALS['rms_extension']['tables'], true)) {
                    $objArticle = ArticleModel::findByPk($id);
                    if (null === $objArticle) {
                        return false;
                    }

                    $objPage = $objArticle->getRelated('pid');
                    if (null === $objPage) {
                        return false;
                    }

                    $objRootPage = $this->getRootPage($objPage->pid);

                    if ('1' === $objRootPage->rms_protected) {
                        if ($objRootPage->rms_master_member > 0) {
                            $this->settings['sender_email'] = $this->getMemberData(
                                $objRootPage->rms_master_member,
                                'email'
                            );
                        }

                        return true;
                    }
                }
                break;
            case 'tl_newsletter':
                // nur wenn das Modul in den Einstellungen von Contao nicht deaktiviert wurde
                if (\in_array('tl_newsletter', $GLOBALS['rms_extension']['tables'], true)) {
                    $objNewsletter = NewsletterModel::findByPk($id);
                    if (null === $objNewsletter) {
                        return false;
                    }

                    $objNewletterChannel = $objNewsletter->getRelated('pid');
                    if (null === $objNewletterChannel) {
                        return false;
                    }

                    if ('1' === $objNewletterChannel->rms_protected) {
                        if ($objNewletterChannel->rms_master_member > 0) {
                            $this->settings['sender_email'] = $this->getMemberData(
                                $objNewletterChannel->rms_master_member,
                                'email'
                            );
                        }

                        return true;
                    }
                }
                break;
            case 'tl_newsletter_channel':
                // nur wenn das Modul in den Einstellungen von Contao nicht deaktiviert wurde
                if (\in_array('tl_newsletter_channel', $GLOBALS['rms_extension']['tables'], true)) {
                    $objNewletterChannel = NewsletterChannelModel::findByPk($id);

                    if ('1' === $objNewletterChannel->rms_protected) {
                        if ($objNewletterChannel->rms_master_member > 0) {
                            $this->settings['sender_email'] = $this->getMemberData(
                                $objNewletterChannel->rms_master_member,
                                'email'
                            );
                        }

                        return true;
                    }
                }
                break;
            case 'tl_faq':
                // nur wenn das Modul in den Einstellungen von Contao nicht deaktiviert wurde
                if (\in_array('tl_faq', $GLOBALS['rms_extension']['tables'], true)) {
                    $objFaq = FaqModel::findByPk($id);
                    if (null === $objFaq) {
                        return false;
                    }

                    $objFaqCategory = $objFaq->getRelated('pid');
                    if (null === $objFaqCategory) {
                        return false;
                    }

                    if ('1' === $objFaqCategory->rms_protected) {
                        if ($objFaqCategory->rms_master_member > 0) {
                            $this->settings['sender_email'] = $this->getMemberData(
                                $objFaqCategory->rms_master_member,
                                'email'
                            );
                        }

                        return true;
                    }
                }
                break;
            case 'tl_faq_category':
                // nur wenn das Modul in den Einstellungen von Contao nicht deaktiviert wurde
                if (\in_array('tl_faq_category', $GLOBALS['rms_extension']['tables'], true)) {
                    $objFaqCategory = FaqCategoryModel::findByPk($id);
                    if (null === $objFaqCategory) {
                        return false;
                    }

                    if ('1' === $objFaqCategory->rms_protected) {
                        if ($objFaqCategory->rms_master_member > 0) {
                            $this->settings['sender_email'] = $this->getMemberData(
                                $objFaqCategory->rms_master_member,
                                'email'
                            );
                        }

                        return true;
                    }
                }
                break;
            case 'tl_news':
                // nur wenn das Modul in den Einstellungen von Contao nicht deaktiviert wurde
                if (\in_array('tl_news', $GLOBALS['rms_extension']['tables'], true)) {
                    $objNews = NewsModel::findByPk($id);
                    if (null === $objNews) {
                        return false;
                    }

                    $objNewsArchive = $objNews->getRelated('pid');
                    if (null === $objNewsArchive) {
                        return false;
                    }

                    if ('1' === $objNewsArchive->rms_protected) {
                        if ($objNewsArchive->rms_master_member > 0) {
                            $this->settings['sender_email'] = $this->getMemberData(
                                $objNewsArchive->rms_master_member,
                                'email'
                            );
                        }

                        return true;
                    }
                }
                break;
            case 'tl_news_archive':
                // nur wenn das Modul in den Einstellungen von Contao nicht deaktiviert wurde
                if (\in_array('tl_news_archive', $GLOBALS['rms_extension']['tables'], true)) {
                    $objNewsArchive = NewsArchiveModel::findByPk($id);

                    if ('1' === $objNewsArchive->rms_protected) {
                        if ($objNewsArchive->rms_master_member > 0) {
                            $this->settings['sender_email'] = $this->getMemberData(
                                $objNewsArchive->rms_master_member,
                                'email'
                            );
                        }

                        return true;
                    }
                }
                break;
            case 'tl_calendar_events':
                // nur wenn das Modul in den Einstellungen von Contao nicht deaktiviert wurde
                if (\in_array('tl_calendar_events', $GLOBALS['rms_extension']['tables'], true)) {
                    $objCalEvent = CalendarEventsModel::findByPk($id);
                    if (null === $objCalEvent) {
                        return false;
                    }
                    $objCal = $objCalEvent->getRelated('pid');

                    if ('1' === $objCal->rms_protected) {
                        if ($objCal->rms_master_member > 0) {
                            $this->settings['sender_email'] = $this->getMemberData(
                                $objCal->rms_master_member,
                                'email'
                            );
                        }

                        return true;
                    }
                }
                break;
            case 'tl_calendar':
                // nur wenn das Modul in den Einstellungen von Contao nicht deaktiviert wurde
                if (\in_array('tl_calendar', $GLOBALS['rms_extension']['tables'], true)) {
                    $objCal = CalendarModel::findByPk($id);
                    if (null === $objCal) {
                        return false;
                    }

                    if ('1' === $objCal->rms_protected) {
                        if ($objCal->rms_master_member > 0) {
                            $this->settings['sender_email'] = $this->getMemberData(
                                $objCal->rms_master_member,
                                'email'
                            );
                        }

                        return true;
                    }
                }
                break;
            default:
                $isProtected = false;

                // HOOK: add custom logic
                if (isset($GLOBALS['TL_HOOKS']['rmsIsTableProtected'])
                    && \is_array($GLOBALS['TL_HOOKS']['rmsIsTableProtected'])) {
                    foreach ($GLOBALS['TL_HOOKS']['rmsIsTableProtected'] as $callback) {
                        if (\is_array($callback)) {
                            $this->import($callback[0]);
                            $isProtected = $this->$callback[0]->{$callback[1]}($strTable);
                        } elseif (\is_callable($callback)) {
                            $isProtected = $callback($strTable);
                        }

                        if (\is_bool($isProtected)) {
                            return $isProtected;
                        }
                    }
                }
        }

        return false;
    }

    public function getSettings(): array
    {
        return $this->settings;
    }

    public function setSettings(array $settings): void
    {
        $this->settings = $settings;
    }

    /**
     * get the RootPage from PageId.
     *
     * @param int
     *
     * @return PageModel|mixed|null
     */
    protected function getRootPage($id = 0)
    {
        $id = (int) $id;
        if ($id > 0) {
            $objPage = PageModel::findByPk($id);
            // return obj or recursive this method
            return ('root' === $objPage->type) ? $objPage : $this->getRootPage($objPage->pid);
        }
    }
}
