<?php
/**
 * Created by contao-rms-bundle
 * Developer: Sven Rhinow (sven@sr-tag.de)
 * Date: 05.05.2020
 */

class RoboFile extends \Robo\Tasks
{

    /**
     * Ruft ale Tests auf
     */
    public function runTests(): void
    {
//        $this->runMyCommand('./tools/phpcpd ../src');
        $this->runMyCommand('./tools/php-cs-fixer --config=../php.cs.dist.php fix');
        $this->runMyCommand('./tools/phpcs --colors --standard=PSR2 ../src --ignore=..*/DC/*');
    }

    /**
     * Fuehrt ein Kommando aus
     * @param $command
     */
    protected function runMyCommand($command): void
    {
        if(!$this->taskexec($command)->run()->wasSuccessful()){
            exit(1);
        }

        $this->say('-------------------------------------------');
    }
}