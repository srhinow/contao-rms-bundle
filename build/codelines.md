## PHP Lines of Code
`build/tools/phploc src`

## PHP Copy And Paste Detector
`build/tools/phpcpd src`

## PHP Code Sniffer
`build/tools/phpcs --colors --standard=PSR2 src`

## PHP CS Fixer
`build/tools/php-cs-fixer --config=build/tools/php.cs.dist.php fix`

## PHP UNIT TESTS
build/tools/phpunit --colors --bootstrap ../../../cendor/autoload.php test/`

## Robo
build/tools/robo -f build/RoboFile.php run:tests
