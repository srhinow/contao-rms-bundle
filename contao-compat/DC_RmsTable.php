<?php
/**
 * Created by rms-contao.
 * Developer: Sven Rhinow (sven@sr-tag.de)
 * Date: 11.04.20
 */

/**
 * COPY FROM DC_General
 *
 * This is the only entry point for Contao to load the DC class.
 *
 * WARNING: Do not move this class into the src folder - the autoloading will clobber then as the psr-4 and classmap
 *          will overlap.
 *
 * @deprecated
 *
 * @SuppressWarnings(PHPMD.CamelCaseClassName)
 */
// @codingStandardsIgnoreStart
class DC_RmsTable extends Srhinow\ContaoRmsBundle\DC\RmsTable
{
}
// @codingStandardsIgnoreEnd